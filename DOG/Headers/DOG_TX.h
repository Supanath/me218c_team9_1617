/****************************************************************************
 
  Header file for DOG TX STATE MACHINE 
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef DOG_TX_H
#define DOG_TX_H

#include "ES_Configure.h"
#include "ES_Types.h"

typedef enum {WaitToPair, PairedWaitForKey, Paired} DogTXState_t;
typedef enum {WaitFor7E, WaitForMSBLen, WaitForLSBLen, GetDataPacket, GetCheckSum} DogRXState_t;
// Public Function Prototypes

bool InitDogTX ( uint8_t Priority );
bool PostDogTX( ES_Event ThisEvent );
ES_Event RunDogTX( ES_Event ThisEvent );
void DOG_UART1__ISR(void);
uint8_t GetRXBuffer(int index);
#endif /* ServTemplate_H */

