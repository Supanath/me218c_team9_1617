/****************************************************************************
 
  Header file for DOG RX Service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef DOG_RX_H
#define DOG_RX_H

#include "ES_Configure.h"
#include "ES_Types.h"

// Public Function Prototypes

bool InitDogRX ( uint8_t Priority );
bool PostDogRX( ES_Event ThisEvent );
ES_Event RunDogRX( ES_Event ThisEvent );
void ResetEncryptKeyIndex(void);
void SetInControlMode(bool input);
uint32_t GetControlMessage();
void ReadID(void);
#endif /* ServTemplate_H */

