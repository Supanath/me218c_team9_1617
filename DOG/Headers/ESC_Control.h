
#ifndef ESC_CONTROL_H
#define ESC_CONTROL_H

#include "ES_Configure.h"
#include "ES_Types.h"

// Public Function Prototypes
uint32_t GetOneShotTimeOut(int input_us);
void InitOneShotInt_ESC_Left( void );
void StartOneShot_ESC_Left(uint32_t input);
void StartOneShot_ESC_10ms_Left( void );
void StartOneShot_ESC_30ms_Left( void );
void OneShotIntResponse_ESC_Left( void );
void SetCurrentTimeout_Left(float input_ms);

void InitOneShotInt_ESC_Right( void );
void StartOneShot_ESC_Right(uint32_t input);
void StartOneShot_ESC_10ms_Right( void );
void StartOneShot_ESC_30ms_Right( void );
void OneShotIntResponse_ESC_Right( void );
void SetCurrentTimeout_Right(float input_ms);
#endif /*SHMTemplate_H */

