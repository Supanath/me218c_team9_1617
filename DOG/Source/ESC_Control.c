/****************************************************************************
 Module
   ESC_Control.c

****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
// Basic includes for a program using the Events and Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "driverlib/gpio.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"	// Define PART_TM4C123GH6PM in project
#include "driverlib/gpio.h"
#include "inc/hw_timer.h"
#include "inc/hw_nvic.h"
#include "ESC_Control.h"
/*----------------------------- Module Defines ----------------------------*/
// define constants for the states for this machine
// and any other local defines

#define NORMAL_OPERATION
//#define TESTING_SUPPLY

#define ENTRY_STATE SUPPLY_WAITING
#define TWO_SEC 2000
#define TEN_MS 10
#define THIRTY_MS 30
#define THREE_SEC 3000
#define HALF_SEC 500
#define MAX_BALL_RECEIVED 4
#define COMPETITION_FULL_DUTY 75
#define CORRECTION_FULL_DUTY 55
/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well

static bool flag_10ms_timer = false;
static bool flag_30ms_timer = false;
static bool flag_3000ms_timer = false;
static int pulse_count = 0;
static bool supply_led_on = false;
static bool loaded_complete = false;
static uint32_t OneShotTimeout_800us = 40000000*0.8/1000;
static uint32_t OneShotTimeout_4ms	= 40000000*4/1000;
static uint32_t OneShotTimeout_20ms	= 40000000*20/1000;
static uint32_t OneShotTimeout_10ms = 40000000*10/1000;
static uint32_t	OneShotTimeout_30ms = 40000000*30/1000;
static uint32_t OneShotTimeout;
static int counter = 0;
static bool count_valid = true;
static bool high_state_left = false;
static bool high_state_right = false;
static const uint32_t LOW_STATE_OFFSET = 20000;
static const uint32_t STOP_CTRL_VALUE	= 800;
static const uint32_t MIN_CTRL_VALUE	= 1000;
static const uint32_t MID_CTRL_VALUE	= 1500;
static const uint32_t MAX_CTRL_VALUE	= 2000;
static const uint32_t CLOCK_RATE = 40000000;
static uint32_t CurrentTimeout_left = 0;
static uint32_t CurrentTimeout_right = 0;

uint32_t GetOneShotTimeOut(int input_us){
		return (CLOCK_RATE*input_us)/1000000;
}

void InitOneShotInt_ESC_Left(void){
		// start by enabling the clock to the timer (Wide Timer 4)
		HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R4;
		// kill a few cycles to let the clock get going
		while((HWREG(SYSCTL_PRWTIMER) & SYSCTL_PRWTIMER_R4) != SYSCTL_PRWTIMER_R4)
		{
		}
		// make sure that timer (Timer B) is disabled before configuring
		HWREG(WTIMER4_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TBEN; //TBEN = Bit8
		// set it up in 32bit wide (individual, not concatenated) mode
		// the constant name derives from the 16/32 bit timer, but this is a 32/64
		// bit timer so we are setting the 32bit mode
		HWREG(WTIMER4_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT; //bits 0-2 = 0x04
		// set up timer B in 1-shot mode so that it disables timer on timeouts
		// first mask off the TAMR field (bits 0:1) then set the value for
		// 1-shot mode = 0x01
		HWREG(WTIMER4_BASE+TIMER_O_TBMR) =
		(HWREG(WTIMER4_BASE+TIMER_O_TBMR)& ~TIMER_TBMR_TBMR_M)|
		TIMER_TBMR_TBMR_1_SHOT;
		// set timeout
		//HWREG(WTIMER4_BASE+TIMER_O_TBILR) = GetOneShotTimeOut(STOP_CTRL_VALUE);
		HWREG(WTIMER4_BASE+TIMER_O_TBILR) = OneShotTimeout_10ms;
		// enable a local timeout interrupt. TBTOIM = bit 8
		HWREG(WTIMER4_BASE+TIMER_O_IMR) |= TIMER_IMR_TBTOIM; // 8
		// enable the Timer B in Wide Timer 0 interrupt in the NVIC
		// it is interrupt number 103 so appears in EN3 at bit 1
		HWREG(NVIC_EN3) |= BIT7HI;
		// make sure interrupts are enabled globally
		__enable_irq();
		//StartTime = ES_Timer_GetTime();
		// now kick the timer off by enabling it and enabling the timer to
		// stall while stopped by the debugger. TAEN = Bit0, TASTALL = bit1
		HWREG(WTIMER4_BASE+TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
		printf("Finished init one shot for Left Fan ESC\r\n");
		SetCurrentTimeout_Left(0.8);
		StartOneShot_ESC_10ms_Left();
}

void StartOneShot_ESC_Left(uint32_t input){
		// start by grabbing the start time
		//StartTime = ES_Timer_GetTime();
		// now kick the timer off by enabling it and enabling the timer to
		// stall while stopped by the debugger
		HWREG(WTIMER4_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TBEN;
		HWREG(WTIMER4_BASE+TIMER_O_TBILR) = input;
		__enable_irq();
		HWREG(WTIMER4_BASE+TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
}


void StartOneShot_ESC_10ms_Left( void ){
		// start by grabbing the start time
		//StartTime = ES_Timer_GetTime();
		// now kick the timer off by enabling it and enabling the timer to
		// stall while stopped by the debugger
		HWREG(WTIMER4_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TBEN;
		HWREG(WTIMER4_BASE+TIMER_O_TBILR) = OneShotTimeout_10ms;
		__enable_irq();
		HWREG(WTIMER4_BASE+TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
}

void StartOneShot_ESC_30ms_Left( void ){
		// start by grabbing the start time
		//StartTime = ES_Timer_GetTime();
		// now kick the timer off by enabling it and enabling the timer to
		// stall while stopped by the debugger
		HWREG(WTIMER4_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TBEN;
		HWREG(WTIMER4_BASE+TIMER_O_TBILR) = OneShotTimeout_30ms;
		__enable_irq();
		HWREG(WTIMER4_BASE+TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
}

void OneShotIntResponse_ESC_Left( void ){
		// start by clearing the source of the interrupt
		HWREG(WTIMER4_BASE+TIMER_O_ICR) = TIMER_ICR_TBTOCINT;
		if(high_state_left){
				GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_0, BIT0LO); //set Control pin High
				StartOneShot_ESC_Left(OneShotTimeout_20ms - CurrentTimeout_left);
				//StartOneShot_ESC(OneShotTimeout_10ms);
				//StartOneShot_ESC_10ms();
				high_state_left = false;
		}
		else{
				GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_0, BIT0HI); //set IR LED Low
				StartOneShot_ESC_Left(CurrentTimeout_left);
				//StartOneShot_ESC_10ms();
				high_state_left = true;
		}
}

void SetCurrentTimeout_Left(float input_ms){
		CurrentTimeout_left = 40000000*input_ms/1000;
}

/*****************************************************************************************/

void InitOneShotInt_ESC_Right( void ){
		// start by enabling the clock to the timer (Wide Timer 5)
		HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R5;
		// kill a few cycles to let the clock get going
		while((HWREG(SYSCTL_PRWTIMER) & SYSCTL_PRWTIMER_R5) != SYSCTL_PRWTIMER_R5)
		{
		}
		// make sure that timer (Timer B) is disabled before configuring
		HWREG(WTIMER5_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TBEN; //TBEN = Bit8
		// set it up in 32bit wide (individual, not concatenated) mode
		// the constant name derives from the 16/32 bit timer, but this is a 32/64
		// bit timer so we are setting the 32bit mode
		HWREG(WTIMER5_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT; //bits 0-2 = 0x04
		// set up timer B in 1-shot mode so that it disables timer on timeouts
		// first mask off the TAMR field (bits 0:1) then set the value for
		// 1-shot mode = 0x01
		HWREG(WTIMER5_BASE+TIMER_O_TBMR) =
		(HWREG(WTIMER5_BASE+TIMER_O_TBMR)& ~TIMER_TBMR_TBMR_M)|
		TIMER_TBMR_TBMR_1_SHOT;
		// set timeout
		HWREG(WTIMER5_BASE+TIMER_O_TBILR) = OneShotTimeout;
		// enable a local timeout interrupt. TBTOIM = bit 8
		HWREG(WTIMER5_BASE+TIMER_O_IMR) |= TIMER_IMR_TBTOIM; // 8
		// enable the Timer B in Wide Timer 5 interrupt in the NVIC
		// it is interrupt number 105 so appears in EN3 at bit 9
		HWREG(NVIC_EN3) |= BIT9HI;
		// make sure interrupts are enabled globally
		__enable_irq();
		//StartTime = ES_Timer_GetTime();
		// now kick the timer off by enabling it and enabling the timer to
		// stall while stopped by the debugger. TAEN = Bit0, TASTALL = bit1
		HWREG(WTIMER5_BASE+TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
		printf("Finished init one shot for Right Fan ESC\r\n");
		SetCurrentTimeout_Right(0.8);
		StartOneShot_ESC_10ms_Right();
}

void StartOneShot_ESC_Right(uint32_t input){
		// start by grabbing the start time
		//StartTime = ES_Timer_GetTime();
		// now kick the timer off by enabling it and enabling the timer to
		// stall while stopped by the debugger
		HWREG(WTIMER5_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TBEN;
		HWREG(WTIMER5_BASE+TIMER_O_TBILR) = input;
		__enable_irq();
		HWREG(WTIMER5_BASE+TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
}


void StartOneShot_ESC_10ms_Right( void ){
		// start by grabbing the start time
		//StartTime = ES_Timer_GetTime();
		// now kick the timer off by enabling it and enabling the timer to
		// stall while stopped by the debugger
		HWREG(WTIMER5_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TBEN;
		HWREG(WTIMER5_BASE+TIMER_O_TBILR) = OneShotTimeout_10ms;
		__enable_irq();
		HWREG(WTIMER5_BASE+TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
}

void StartOneShot_ESC_30ms_Right( void ){
		// start by grabbing the start time
		//StartTime = ES_Timer_GetTime();
		// now kick the timer off by enabling it and enabling the timer to
		// stall while stopped by the debugger
		HWREG(WTIMER5_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TBEN;
		HWREG(WTIMER5_BASE+TIMER_O_TBILR) = OneShotTimeout_30ms;
		__enable_irq();
		HWREG(WTIMER5_BASE+TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
}

void OneShotIntResponse_ESC_Right( void ){
		// start by clearing the source of the interrupt
		HWREG(WTIMER5_BASE+TIMER_O_ICR) = TIMER_ICR_TBTOCINT;
		if(high_state_right){
				GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_3, BIT3LO); //set Control pin High
				StartOneShot_ESC_Right(OneShotTimeout_20ms - CurrentTimeout_right);
				//StartOneShot_ESC(OneShotTimeout_10ms);
				//StartOneShot_ESC_10ms();
				high_state_right = false;
		}
		else{
				GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_3, BIT3HI); //set IR LED Low
				StartOneShot_ESC_Right(CurrentTimeout_right);
				//StartOneShot_ESC_10ms();
				high_state_right = true;
		}
}

void SetCurrentTimeout_Right(float input_ms){
		CurrentTimeout_right = 40000000*input_ms/1000;
}