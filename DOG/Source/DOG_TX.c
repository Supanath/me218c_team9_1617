/****************************************************************************
Definition for debugging
 ***************************************************************************/
//#define DEBUGGING_STATE 			// print out farmer's current state
//#define DEBUGGING_EVENT 			// print out current event
#define DEMO_MANUAL_MODE 			// trigger event by keyboard input
//#define DEBUGGING_ENCRYPTION_KEY 	// print out encryption key
//#define CALCULATE_1
#define CALCULATE_2
#define AUTOMATIC_POW_RELAY_ON
//#define DEBUGGING_IMU_DATA		// print out IMU value
#define NORMAL_OP_MODE 			// trigger event from controller input and timer
//#define DEBUGGING_TX_DATA 		// print out transfer data packet
//#define DEBUGGING_SAMPLE_DATA 	// use sample data packet for transfer

/* To do for DOG
1. Update Execute command method
*/

/****************************************************************************
 Module
   DOG_TX.c

 Description
   This is the top service for DOG which acts as a controller of the DOG
	 as well as its transmitter module

****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
/* include header files for the framework and this service
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "DOG_TX.h"
#include "UART_Init.h"
#include "DOG_RX.h"
#include "XBEE_DEFS.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"	// Define PART_TM4C123GH6PM in project
#include "driverlib/gpio.h"
#include "ES_ShortTimer.h"
#include "inc/hw_types.h"
#include "inc/hw_uart.h"
#include "inc/hw_nvic.h"
#include "inc/hw_timer.h"
#include "IMU_Service_SPI.h"
#include "ESC_Control.h"
#include "Servo_Control.h"
#include <stdlib.h>
#include "AnalogRead.h"
/*----------------------------- Module Defines ----------------------------*/
// these times assume a 1.000mS/tick timing
#define ONE_SEC 1000
#define HALF_SEC (ONE_SEC/2)
#define TWO_SEC (ONE_SEC*2)
#define FIVE_SEC (ONE_SEC*5)
#define LOST_COMM_TIME ONE_SEC
#define DIRECTION_CHANGE_TIME 100

// packet byte definition
#define	START_BYTE 						0x7E
#define API_TX_ID 						0x01
#define OPTION_BYTE 					0x00
#define	MSB_LEN 							0x00
#define MAX_XBEE_PACKET_SIZE 	9
#define PAIR_RESPONSE_DATA_LEN 1
#define CHECKSUM_OFFSET				0xFF
#define XBEE_OVERHEAD_LEN			9
#define DATA_OVERHEAD_LEN			5
#define LARGEST_FRAME_DATA 		100
#define ACCEL_X_INDEX					0
#define ACCEL_Y_INDEX					1
#define ACCEL_Z_INDEX					2
#define GYRO_X_INDEX					3
#define GYRO_Y_INDEX					4
#define GYRO_Z_INDEX					5
#define	MSB_MASK							0xFF00
#define LSB_MASK							0x00FF
#define BRAKE_MASK						0x02
#define SPECIAL_FUNC_MASK			0x01
#define BRAKE_SERVO_CHANNEL		2
#define UNBRAKE_ANGLE 				65
#define BRAKE_ANGLE 					90
#define FORWARD_DIR 					1
#define BACKWARD_DIR 					2
#define AD_CHANNEL_NUM				1
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static bool TransmitPacket();
static void ClearDataPacket(void);
static void WriteToUART(void);
static void ConstructPacket(void);
static void TurnFanOn(void);
static void TurnFanOff(void);
static void SetFarmerAddress(uint16_t input);
static void ResetFarmerAddress(void);
static void Send_PairResponse(void);
static void Send_StatusMessage(void);
static void Send_EncryptionReset(void);
static void ExecuteCommand(uint32_t cmd);
static void ConStructStatusMessage(void);
static void Init_Port(void);
static void StopMotor_Left(void);
static void SetMotor_Left(float input);
static void StopMotor_Right(void);
static void SetMotor_Right(float input);
static void SetPowerRelay(bool input);
static void SetLeftFan_Forward();
static void SetRightFan_Forward();
static void SetLeftFan_Backward();
static void SetRightFan_Backward();
static void Calculate_Motor_Speed(uint8_t cmd1, uint8_t cmd2);
static void SetBrakeOn(void);
static void SetBrakeOff(void);
static void SetSpecialFnOn(void);
static void SetSpecialFnOff(void);
static float MapToPeriod(float input);
static void ClearRXDataPacket(void);
static void ClearRXDataPacket(void);
static void ClearRXBuffer(void);
static void CopyToBuffer(void);
static void ReadDogTag(void);
static void SwitchSong(void);
/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
DogTXState_t CurrentState;	//define currentstate variable for this SM
DogRXState_t CurrentRXState;	//define currentstate variable for this SM
static uint8_t DataPacket[LARGEST_FRAME_DATA]; //define datapacket array
static uint8_t XbeePacket[LARGEST_FRAME_DATA + MAX_XBEE_PACKET_SIZE];
static uint8_t DataPacket_Length;
static uint8_t XbeePacket_Length;
static uint8_t CheckSum = 0x00;
static uint8_t frame_id = 0x01;
static uint8_t byte_remaining = 0x00;
static uint8_t MSB_RX_ADDRESS = 0x00;
static uint8_t LSB_RX_ADDRESS = 0x00;
static uint8_t FanState = 0x01;
static uint8_t ACCEL_X_MSB = 0x01;
static uint8_t ACCEL_X_LSB = 0x02;
static uint8_t ACCEL_Y_MSB = 0x03;
static uint8_t ACCEL_Y_LSB = 0x04;
static uint8_t ACCEL_Z_MSB = 0x05;
static uint8_t ACCEL_Z_LSB = 0x06;
static uint8_t GYRO_X_MSB = 0x07;
static uint8_t GYRO_X_LSB = 0x08;
static uint8_t GYRO_Y_MSB = 0x09;
static uint8_t GYRO_Y_LSB = 0x0a;
static uint8_t GYRO_Z_MSB = 0x0b;
static uint8_t GYRO_Z_LSB = 0x0c;
static int index = 0;
static const int STATUS_MESSAGE_LEN = 13;
static const int ENCR_RESET_LEN = 1;
static uint16_t paired_farmer_address = 0x2189;
static uint16_t ACCEL_X_VALUE = 0;
static uint16_t ACCEL_Y_VALUE = 0;
static uint16_t ACCEL_Z_VALUE = 0;
static uint16_t GYRO_X_VALUE = 0;
static uint16_t GYRO_Y_VALUE = 0;
static uint16_t GYRO_Z_VALUE = 0;

static const float MAX_CTRL_OFFSET = 1.0;
static const float STOP_CTRL_VALUE	= 0.8;
static const float MIN_CTRL_VALUE	= 1;
static const float MID_CTRL_VALUE	= 1.4;
static const float MAX_CTRL_VALUE	= 1.8;
static const float alpha_multiplier = 0.5;
static const float beta_multiplier = 1 - alpha_multiplier;
static float left_pow = 0.0;
static float right_pow = 0.0;
static int Ctrl_Offset = 127;
static int Ctrl_Stop_Tol = 15;

/* Received side parameters */
static uint8_t RX_DataPacket_Length;
static uint8_t RX_CheckSum = 0x00;
static uint8_t Received_RX_CheckSum = 0x00;
static uint8_t RX_Message_MSB_Len = 0;
static uint8_t RX_Message_LSB_Len = 0; 
static uint8_t RX_DataPacket[LARGEST_FRAME_DATA] = {0}; //define datapacket array
static uint8_t RX_Buffer_Array[LARGEST_FRAME_DATA] = {0}; //define datapacket array
static int rx_index = 0;

static bool change_dir_complete = true;
static int prev_direction_right = 0;
static int prev_direction_left = 0;
static int cur_direction_right = 0;
static int cur_direction_left = 0;
static uint32_t current_cmd = 0;
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitDogTX

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any 
     other required initialization for this service

****************************************************************************/
bool InitDogTX ( uint8_t Priority )
{
		ES_Event ThisEvent;
		
		MyPriority = Priority;
	
		/********************************************
		 Initialization Code
		 *******************************************/
	
		// set up I/O lines for debugging
		SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
		GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_2);
	
		// start with the lines low
		GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_2, BIT2LO);  
	
		// initialize current state to waiting to pair
		CurrentState = WaitToPair;
	  CurrentRXState = WaitFor7E;
		
		// Initialize variable to 0
		DataPacket_Length = 0;
		XbeePacket_Length = 0;

		// Initialize UART
		Init_UART();	// this is for Xbee
		Init_UART_ModuleFive(); // this is for talking to TREAT
		
		// Initialize Servo
		InitServo();
		
		// Init PF0 and one shot timer for ESC control
		Init_Port();
		// Initialize one shot interrupt for ESC left and right
		InitOneShotInt_ESC_Left();
		InitOneShotInt_ESC_Right();
		
		// Initialize A/D Channel
		ADC_MultiInit(AD_CHANNEL_NUM);
		
		// Release brake
		SetBrakeOff();
		
		// also need to enable NVIC and enable global interrupt
		HWREG(NVIC_EN0) |= BIT6HI; // from page 104, this is for UART1 (vector 22, bit 6)
		__enable_irq();
		
		// turn power relay off
		#ifdef AUTOMATIC_POW_RELAY_ON
		SetPowerRelay(true);
		#endif
		
		// turning fan off initially
		TurnFanOff();
		
		// print out initializing completion
		printf("Finished initializing Dog TX Service\r\n");

		// post the initial transition event
		ThisEvent.EventType = ES_INIT;
		if (ES_PostToService( MyPriority, ThisEvent) == true)
		{
				return true;
		}else
		{
				return false;
		}
}

/****************************************************************************
 Function
     PostDogTX

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue

****************************************************************************/
bool PostDogTX( ES_Event ThisEvent )
{
  return ES_PostToService( MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunDogTX

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here

****************************************************************************/
ES_Event RunDogTX( ES_Event ThisEvent )
{
  ES_Event ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  DogTXState_t NextState = CurrentState;
  
  switch (CurrentState){
			// if current state is wait to pair
			case WaitToPair:
					// if receive pair request message from the FARMER
					if(ThisEvent.EventType == PAIR_REQUEST_RECEIVED){
							#ifdef DEBUGGING_EVENT
							printf("DOG: Received pairing request message through Zigbee\r\n");
							#endif
							// reset farmer address
							ResetFarmerAddress();
							// set farmer address
							SetFarmerAddress(ThisEvent.EventParam);
							// Transmit pair ack packet
							Send_PairResponse();
					}
					// if receive ack for pair response message
					else if((ThisEvent.EventType == ACK_RECEIVED) && (ThisEvent.EventParam == PAIR_RESPONSE_FRAME_ID)){
							// start 1s incoming transmission time
							ES_Timer_InitTimer(LOST_COMM_TIMER, LOST_COMM_TIME);
							// turn on Treat fan
							TurnFanOn();
							// set next state to paired wait for key
							NextState = PairedWaitForKey;
							#ifdef DEBUGGING_STATE
							printf("DOG state: PAIRED WAIT FOR KEY\r\n");
							#endif
					}
					// if receive nack for pair response message
					else if((ThisEvent.EventType == NACK_RECEIVED)&& (ThisEvent.EventParam == PAIR_RESPONSE_FRAME_ID)){
							// resend pair response message
							Send_PairResponse();
					}
					
					// the following code is for testing fan control and motor control
					#ifdef DEMO_MANUAL_MODE 
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == 'o')){
							// turn on Treat fan
							TurnFanOn();
					}
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == 'p')){
							// turn off Treat fan
							TurnFanOff();
					}
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == 's')){
							// stop motor
							StopMotor_Left();
							StopMotor_Right();
					}
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == 'd')){
							// set motor speed minimum
							SetMotor_Left(MIN_CTRL_VALUE);
							SetMotor_Right(MIN_CTRL_VALUE);
					}
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == 'f')){
							// set motor speed maximum
							SetMotor_Left(MID_CTRL_VALUE);
							SetMotor_Right(MID_CTRL_VALUE);
					}
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == 'g')){
							// set motor speed maximum
							SetMotor_Left(MAX_CTRL_VALUE);
							SetMotor_Right(MAX_CTRL_VALUE);
					}
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == 'q')){
							// set motor speed maximum
							SetPowerRelay(false);
					}
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == 'w')){
							// set motor speed maximum
							SetPowerRelay(true);
					}
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == 'n')){
							// set motor speed maximum
							SetLeftFan_Forward();
							SetRightFan_Forward();
					}
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == 'm')){
							// set motor speed maximum
							SetLeftFan_Backward();
							SetRightFan_Backward();
					}
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == 'l')){
							Calculate_Motor_Speed(255, 255);
							Calculate_Motor_Speed(255, 0);
							Calculate_Motor_Speed(255, 127);
							Calculate_Motor_Speed(127, 255);
							Calculate_Motor_Speed(0, 255);
							Calculate_Motor_Speed(0, 0);
							Calculate_Motor_Speed(0, 127);
							Calculate_Motor_Speed(127,127);
					}
					
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == 'b')){
							SetBrakeOn();
					}
					
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == 'v')){
							SetBrakeOff();
					}
					
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == '1')){
							ExecuteCommand(0x00FFFF00);
					}
					
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == '2')){
							ExecuteCommand(0x00FF0000);
					}
					
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == '3')){
							ExecuteCommand(0x00FF7F00);
					}
					
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == '4')){
							ExecuteCommand(0x007FFF00);
					}
					
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == '5')){
							ExecuteCommand(0x0000FF00);
					}
					
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == '6')){
							ExecuteCommand(0x00000000);
					}
					
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == '7')){
							ExecuteCommand(0x00007F00);
					}
					
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == '8')){
							ExecuteCommand(0x007F7F00);
					}
					
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == '9')){
							ReadID();
					}
					#endif
					
					break;
					
			// if current state is paired wait for key
			case PairedWaitForKey:\
					// if don't hear back from Farmer with in 1 second
					if((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == LOST_COMM_TIMER)){
							#ifdef DEBUGGING_STATE
							printf("DOG: One second timeout, return to WaitToPair\r\n");
							#endif
							// turn off Treat Fan
							TurnFanOff();
							// if timeout, go back to wait to pair state
							NextState = WaitToPair;
							// Disable Lost Communication timer
							ES_Timer_StopTimer(LOST_COMM_TIMER);
							#ifdef DEBUGGING_STATE
							printf("DOG STATE: WaitForKey --> WaitToPair\r\n");
							#endif
					}
					
					// if receive encryption key message
					if(ThisEvent.EventType ==  ENCRYPTION_KEY_RECEIVED){
							// encryption keys are stored in DOG_RX
							// restart lost comm timer
							ES_Timer_InitTimer(LOST_COMM_TIMER, LOST_COMM_TIME);
							// set RX_state to be in control mode (all received data will first be decrypted)
							SetInControlMode(true);
							// set next state to paired
							NextState = Paired;
							// switch song
							SwitchSong();
							#ifdef DEBUGGING_STATE
							printf("DOG STATE: Paired with %.4x\r\n", ThisEvent.EventParam);
							#endif
					}
					break;
			
			// if current state is paired
			case Paired:
					#ifdef NORMAL_OP_MODE
					// if 1s timeout
					if((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == LOST_COMM_TIMER)){
							// set RX state to not be in control mode, rx packet will not be decrypted
							SetInControlMode(false);
							#ifdef DEBUGGING_EVENT
							printf("DOG: One second timeout, return to WaitToPair\r\n");
							#endif
							// turn off Treat Fan
							TurnFanOff();
							// if timeout, go back to wait to pair state
							NextState = WaitToPair;
							// switch song
							SwitchSong();
							// reset encryption index key
							ResetEncryptKeyIndex();
							// Stop motor
							StopMotor_Left();
							StopMotor_Right();
							// Disable Lost Communication timer
							ES_Timer_StopTimer(LOST_COMM_TIMER);
							#ifdef DEBUGGING_STATE
							printf("DOG STATE: Paired --> WaitToPair\r\n");
							#endif
					}
					
					// if this is direction change timer timeout, set change direction variable and execute current command
					if((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == DIRECTION_CHANGE_TIMER)){
							change_dir_complete = true;
							ExecuteCommand(current_cmd);
					}
					#endif
					
					#ifdef MANUAL_OP_MODE
					// if x is pressed
					if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == 'x')){
							// set RX state to not be in control mode, rx packet will not be decrypted
							SetInControlMode(false);
							#ifdef DEBUGGING_EVENT
							printf("DOG: One second timeout, return to WaitToPair\r\n");
							#endif
							// turn off Treat Fan
							TurnFanOff();
							// switch song
							SwitchSong();
							// if timeout, go back to wait to pair state
							NextState = WaitToPair;
							// reset encryption index key
							ResetEncryptKeyIndex();
							// Disable Lost Communication timer
							ES_Timer_StopTimer(LOST_COMM_TIMER);
							#ifdef DEBUGGING_STATE
							printf("DOG STATE: WaitToPair\r\n");
							#endif
					}
					#endif
					
					// if received control message
					if((ThisEvent.EventType == CTRL_COMMAND_RECEIVED) && change_dir_complete){
							// restart lost comm timer
							ES_Timer_InitTimer(LOST_COMM_TIMER, LOST_COMM_TIME);
							// Set current command
							current_cmd = GetControlMessage();
							// excecute command
							ExecuteCommand(current_cmd);
							// transmit status
							Send_StatusMessage();
					}
					// if received invalid control message
					else if(ThisEvent.EventType == INVALID_CTRL_COMMAND_RECEIVED){
							// make sure rx state is still in control mode
							//SetInControlMode(false);
							// restart lost comm timer
							ES_Timer_InitTimer(LOST_COMM_TIMER, LOST_COMM_TIME);
							// transmit reset encryption index request
							ResetEncryptKeyIndex();
							// send encryption key reset message to farmer
							Send_EncryptionReset();
							// Stop motor
							StopMotor_Left();
							StopMotor_Right();
					}
					
					// if receive nack for the control message
					else if((ThisEvent.EventType == ACK_RECEIVED) && (ThisEvent.EventParam == frame_id)){
							// test restart lost comm timer when getting ack
							ES_Timer_InitTimer(LOST_COMM_TIMER, LOST_COMM_TIME);
							#ifdef DEBUGGING_EVENT
							printf("Correctly received control message ack with correct frame id: %.2x\r\n", frame_id);
							#endif
					}
					
					// if receive nack for the control message
					else if((ThisEvent.EventType == NACK_RECEIVED) && (ThisEvent.EventParam == frame_id)){
							// test restart lost comm timer when getting nack
							ES_Timer_InitTimer(LOST_COMM_TIMER, LOST_COMM_TIME);
							// send control packet
							Send_StatusMessage();
							#ifdef DEBUGGING_EVENT
							printf("Incorrectly received control message nack with correct frame id: %.2x\r\n", frame_id);
							#endif
					}
					break;

			default :
				break;
  }
	// update current state
	CurrentState = NextState;
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

static void StopMotor_Left(){
		printf("Stop motor\r\n");
		SetCurrentTimeout_Left(STOP_CTRL_VALUE);
}

static void SetMotor_Left(float input){
		printf("Set pulse period to: %f ms\r\n", input);
		SetCurrentTimeout_Left(input);
}

static void StopMotor_Right(){
		printf("Stop motor\r\n");
		SetCurrentTimeout_Right(STOP_CTRL_VALUE);
}

static void SetMotor_Right(float input){
		printf("Set pulse period to: %f ms\r\n", input);
		SetCurrentTimeout_Right(input);
}

static void SetPowerRelay(bool input){
		if(input){
			printf("Turn on Power Relay\r\n");
			GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_4, BIT4HI);
		}
		else{
			printf("Turn off Power Relay\r\n");
			GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_4, BIT4LO);
		}
}

static void SetLeftFan_Backward(){
		printf("Switch Left Fan Relay: Backward\r\n");
		GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, BIT1LO);
}

static void SetLeftFan_Forward(){
		printf("Switch Left Fan Relay: Forward\r\n");
		GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, BIT1HI);
}

static void SetRightFan_Backward(){
		printf("Switch Right Fan Relay: Backward\r\n");
		GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, BIT2LO);
}

static void SetRightFan_Forward(){
		printf("Switch Right Fan Relay: Forward\r\n");
		GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, BIT2HI);
}

static void SetBrakeOn(){
		printf("Brake On\r\n");
		moveToAngle(BRAKE_ANGLE, BRAKE_SERVO_CHANNEL);
}

static void SetBrakeOff(){
		printf("Brake Off\r\n");
		moveToAngle(UNBRAKE_ANGLE, BRAKE_SERVO_CHANNEL);
}

static void SetSpecialFnOn(){
		printf("If you read this, you are special.\r\n");
}

static void SetSpecialFnOff(){
		printf("If you read this, you are also special.\r\n");
}

static void SwitchSong(){
	GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_2, BIT2HI);
	printf("This is a delay message to make some time for dog\r\n");
	GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_2, BIT2LO);
}

/*
 *	This function write 0x01 to UART5 to turn fan on
 */
static void TurnFanOn(){
			printf("Turn fan on\r\n");
			if((HWREG(UART5_BASE + UART_O_FR) & UART_FR_TXFE) == UART_FR_TXFE){
				// write 0x01 to PE5 UART to turn on fan
				FanState = 0x01;
			}
			// write data to UART
			HWREG(UART5_BASE + UART_O_DR) = (HWREG(UART5_BASE + UART_O_DR) & ~UART_DR_DATA_M) + FanState;
}

/*
 *	This function write 0x00 to UART5 to turn fan on
 */
static void TurnFanOff(){
			printf("Turn fan off\r\n");
			if((HWREG(UART5_BASE + UART_O_FR) & UART_FR_TXFE) == UART_FR_TXFE){
				// write 0x00 to PE5 UART to turn off fan
				FanState = 0x00;
			}
			// write data to UART
			HWREG(UART5_BASE + UART_O_DR) = (HWREG(UART5_BASE + UART_O_DR) & ~UART_DR_DATA_M) + FanState;
}

/*
 *	This function clears farmer address
 */
static void ResetFarmerAddress(){
		paired_farmer_address = 0x0000;
}

/*
 *	This function sets farmer address
 */
static void SetFarmerAddress(uint16_t input){
		paired_farmer_address = input;
}

/*
 *	This function execute the control command sent by farmer
 */
static void ExecuteCommand(uint32_t cmd){
		#ifdef DEBUGGING_EVENT
		printf("Executing control command\r\n");
		#endif
		printf("Execute command: %.8x\r\n", cmd);
		uint8_t cmd1 = (cmd & 0x00ff0000) >> 16; // forward and backward cmd
		uint8_t cmd2 = (cmd & 0x0000ff00) >> 8;  // left and right cmd
		uint8_t cmd3 = (cmd & 0x000000ff);		 // digital cmd
		printf("Control 1 is: %.2x\r\n", cmd1);
	
		// first calculate the left and right power value for the motors
		Calculate_Motor_Speed(cmd1, cmd2);
		// set left fan direction and magnitude
		if(left_pow < 0){
			cur_direction_left = BACKWARD_DIR;
		}
		else{
			cur_direction_left = FORWARD_DIR;
		}
		
		// set right fan direction and magnitude
		if(right_pow < 0){
			cur_direction_right = BACKWARD_DIR;
		}
		else{
			cur_direction_right = FORWARD_DIR;
		}

		// check if new direction is different than the previous direction
		if((cur_direction_left != prev_direction_left) || (cur_direction_right != prev_direction_right)){
			StopMotor_Left();
			StopMotor_Right();
			ES_Timer_InitTimer(DIRECTION_CHANGE_TIMER, DIRECTION_CHANGE_TIME);
			change_dir_complete = false;
		}
		else{
			// set left fan direction and magnitude
			if(left_pow < 0){
				SetLeftFan_Backward();
				SetMotor_Left(MapToPeriod(left_pow*-1));
			}
			else{
				SetLeftFan_Forward();
				SetMotor_Left(MapToPeriod(left_pow));
			}
			
			// set right fan direction and magnitude
			if(right_pow < 0){
				SetRightFan_Backward();
				SetMotor_Right(MapToPeriod(right_pow*-1));
			}
			else{
				SetRightFan_Forward();
				SetMotor_Right(MapToPeriod(right_pow));
			}
		}
		
		// update previous direction
		prev_direction_left = cur_direction_left;
		prev_direction_right = cur_direction_right;
		
		// Execute brake
		if((cmd3 & BRAKE_MASK) == BRAKE_MASK){
			SetBrakeOn();
		}
		else{
			SetBrakeOff();
		}

		// Execute Special function
		if((cmd3 & SPECIAL_FUNC_MASK) == SPECIAL_FUNC_MASK){
			SetSpecialFnOn();
		}
		else{
			SetSpecialFnOff();
		}
}

static void Calculate_Motor_Speed(uint8_t cmd1, uint8_t cmd2){
		printf("CMD1 is: %d\r\n", cmd1);
		printf("CMD2 is: %d\r\n", cmd2);
	
		// is the control value is between the stop window, set it to 127
		if((cmd1 <= Ctrl_Offset + Ctrl_Stop_Tol) && (cmd1 >= Ctrl_Offset - Ctrl_Stop_Tol)){
			cmd1 = Ctrl_Offset;
		}
		
		#ifdef CALCULATE_1
		// magnitude of the power
		float alpha = ((cmd1 - 127)*100)/127;
		// beta in this place is the differnce between left and right 
		float beta = ((cmd2 -127)*100)/127;
		printf("Alpha is: %f\r\n", alpha);
		printf("Beta is: %f\r\n", beta);
		left_pow = ((alpha-beta)/2);
		right_pow = ((alpha+beta)/2);
		printf("Left power is: %f\r\n", left_pow);
		printf("Right power is: %f\r\n", right_pow);
		#endif
		
		#ifdef CALCULATE_2
		if(cmd1 == 127 && cmd2 == 127){
			left_pow = 0;
			right_pow = 0;
		}
		else{
			// magnitude of the power
			float alpha = ((float)cmd1 - 127)/127;
			// beta in this place is the differnce between left and right 
			float beta = ((float)cmd2 -127)/127;
			float abs_alpha = alpha;
			float abs_beta = beta;
			
			// calculate the absolute value of alpha and beta
			if(alpha < 0){
				abs_alpha *= -1;
			}
			if(beta < 0){
				abs_beta *= -1;
			}
			
			left_pow = 100 * (alpha*abs_alpha + beta*abs_beta)/(abs_alpha+abs_beta);
			right_pow = 100 * (alpha*abs_alpha - beta*abs_beta)/(abs_alpha+abs_beta);
		}
		printf("Left power is: %f\r\n", left_pow);
		printf("Right power is: %f\r\n", right_pow);
		#endif
		printf("*************************\r\n");
}

static float MapToPeriod(float input){
		// mapping input to output (0.8 is the base value with maximum value of 1.5)
		return (input*MAX_CTRL_OFFSET)/100 +  STOP_CTRL_VALUE;
}

/*
 *	This function construct a pair response message
 */
static void Send_PairResponse(){
		#ifdef DEBUGGING_EVENT
		printf("DOG: Send pairing response message\r\n");
		#endif
		// first clear the data packet
		ClearDataPacket();
		// request to pair packet has length of 2 bytes
		DataPacket_Length = PAIR_RESPONSE_DATA_LEN;
		// first byte is the header
		DataPacket[0] = PAIR_ACK;
		// set Destination address
		MSB_RX_ADDRESS = (paired_farmer_address >> 8);
		LSB_RX_ADDRESS = (paired_farmer_address & 0x00ff);
		// set frame id
		frame_id = PAIR_RESPONSE_FRAME_ID;
		// construct packet
		ConstructPacket();
		// call transmit function to transmit data
		TransmitPacket();
}

/*
 *	This function construct a status xbee message
 */
static void Send_StatusMessage(){
		#ifdef DEBUGGING_EVENT
		printf("DOG: Send status report message\r\n");
		#endif
		// first clear the data packet
		ClearDataPacket();
		// request to pair packet has length of 2 bytes
		DataPacket_Length = STATUS_MESSAGE_LEN;
		// first byte is the header
		ConStructStatusMessage();
		// set Destination address
		MSB_RX_ADDRESS = (paired_farmer_address >> 8);
		LSB_RX_ADDRESS = (paired_farmer_address & 0x00ff);
		// increment frame id by 1 every time we send status
		frame_id ++;
		// construct packet
		ConstructPacket();
		// call transmit function to transmit data
		TransmitPacket();
}

/*
 *	This function construct an encryption reset packet
 */
static void Send_EncryptionReset(){
		#ifdef DEBUGGING_EVENT
		printf("DOG: Send reset encryption message\r\n");
		#endif
		// first clear the data packet
		ClearDataPacket();
		// request to pair packet has length of 2 bytes
		DataPacket_Length = ENCR_RESET_LEN;
		// first byte is the header
		DataPacket[0] = ENCR_RESET;
		// set frame id
		frame_id = ENCR_RESET_FRAME_ID;
		// set Destination address
		MSB_RX_ADDRESS = (paired_farmer_address >> 8);
		LSB_RX_ADDRESS = (paired_farmer_address & 0x00ff);
		// construct packet
		ConstructPacket();
		// call transmit function to transmit data
		TransmitPacket();
}

/*
 *	This function construct a status data packet
 */
static void ConStructStatusMessage(){
		// read the value from the IMU Service module
		ACCEL_X_VALUE = GetIMUData(ACCEL_X_INDEX);
		ACCEL_Y_VALUE = GetIMUData(ACCEL_Y_INDEX);
		ACCEL_Z_VALUE = GetIMUData(ACCEL_Z_INDEX);
		GYRO_X_VALUE = GetIMUData(GYRO_X_INDEX);
		GYRO_Y_VALUE = GetIMUData(GYRO_Y_INDEX);
		GYRO_Z_VALUE = GetIMUData(GYRO_Z_INDEX);
	
		#ifdef DEBUGGING_IMU_DATA
		printf("accel x value: %.4x\r\n", ACCEL_X_VALUE);
		printf("accel y value: %.4x\r\n", ACCEL_Y_VALUE);
		printf("accel z value: %.4x\r\n", ACCEL_Z_VALUE);
		printf("gyro x value: %.4x\r\n", GYRO_X_VALUE);
		printf("gyro y value: %.4x\r\n", GYRO_Y_VALUE);
		printf("gyro z value: %.4x\r\n", GYRO_Z_VALUE);
		#endif
		
		// construct the packet
		DataPacket[0] = STATUS;
		DataPacket[1] = (ACCEL_X_VALUE & MSB_MASK) >> 8;
		DataPacket[2] = (ACCEL_X_VALUE & LSB_MASK);
		DataPacket[3] = (ACCEL_Y_VALUE & MSB_MASK) >> 8;
		DataPacket[4] = (ACCEL_Y_VALUE & LSB_MASK);
		DataPacket[5] = (ACCEL_Z_VALUE & MSB_MASK) >> 8;
		DataPacket[6] = (ACCEL_Z_VALUE & LSB_MASK);
		DataPacket[7] = (GYRO_X_VALUE & MSB_MASK) >> 8;
		DataPacket[8] = (GYRO_X_VALUE & LSB_MASK);
		DataPacket[9] = (GYRO_Y_VALUE & MSB_MASK) >> 8;
		DataPacket[10] = (GYRO_Y_VALUE & LSB_MASK);
		DataPacket[11] = (GYRO_Z_VALUE & MSB_MASK) >> 8;
		DataPacket[12] = (GYRO_Z_VALUE & LSB_MASK);
}

/*
 *	This function construct a packet to be transmitted with option of
 *	broadcasting to all listenning devices
 */
static void ConstructPacket(){
		XbeePacket[0] = START_BYTE;
		XbeePacket[1] = MSB_LEN;
		XbeePacket[2] = DataPacket_Length + DATA_OVERHEAD_LEN;
		XbeePacket[3] = API_TX_ID;
		CheckSum = API_TX_ID; // start increment checksum
		XbeePacket[4] = frame_id;
		CheckSum += frame_id;
		XbeePacket[5] = MSB_RX_ADDRESS;
		CheckSum += MSB_RX_ADDRESS;
		XbeePacket[6] = LSB_RX_ADDRESS;
		CheckSum += LSB_RX_ADDRESS;
		XbeePacket[7] = OPTION_BYTE;
		CheckSum += OPTION_BYTE;
		// now move data packet to the xbee packet
		for(int i = 0; i < DataPacket_Length; i++){
			XbeePacket[i+8] = DataPacket[i];
			CheckSum += DataPacket[i];
		}
		// calculate checksum value
		XbeePacket[DataPacket_Length + 8] = CHECKSUM_OFFSET - CheckSum;
		//frame_id += 1; // increment fram id number
		XbeePacket_Length = DataPacket_Length + XBEE_OVERHEAD_LEN;
}

/*
 *	This function clear the content of data packet array
 */
static void ClearDataPacket(){
		for(int i = 0; i < LARGEST_FRAME_DATA; i++){
			// set all index of data packet to 0
			DataPacket[i] = i;
		}
}

/*
 *	This function transmit packet by start writing to UART pin out
 *  We need to call ConstructPacket before calling this function
 */
static bool TransmitPacket(){
		// set remaining number of byte to transmit to the xbee packet length
		byte_remaining = XbeePacket_Length;
		// reset index to 0
		index = 0;
		
		// check if UART Transmit FIFO empty bit is set, this mean we can send
		if((HWREG(UART1_BASE + UART_O_FR) & UART_FR_TXFE) == UART_FR_TXFE){
			// write data to UART
			WriteToUART();
			
			// this if loop is for increasing effciency of trasmitting
			if ((HWREG(UART1_BASE + UART_O_FR) & UART_FR_TXFE) == UART_FR_TXFE){
				// write data to UART
				WriteToUART();
			}
			//enable TX interrupt
			HWREG(UART1_BASE + UART_O_IM) |= UART_IM_TXIM;
			//return success
			return true;
		}
		else{
			// else return failure
			return false;
		}
}

/*
 *	This function copy data from xbeepacket array to the UART pin out
 */
static void WriteToUART(){
		// write XbeePacket[tx_index] to UARTDR
		HWREG(UART1_BASE + UART_O_DR) = (HWREG(UART1_BASE + UART_O_DR) & ~UART_DR_DATA_M) + XbeePacket[index];
		// decrement byte remaining
		byte_remaining--;
		// increment index
		index++;
}

static void Init_Port(){
		//Initialize Port F
		SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	
		//Initialize PF0 for left fan ESC Control
	  HWREG(GPIO_PORTF_BASE + GPIO_O_PUR) = HWREG(GPIO_PORTF_BASE + GPIO_O_PUR) | (1<<0);
    HWREG(GPIO_PORTF_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY;
    HWREG(GPIO_PORTF_BASE + GPIO_O_CR) = (1<<0);
		GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_0);
	
		//Initialize PF1 for left fan direction relay
		HWREG(GPIO_PORTF_BASE + GPIO_O_PUR) = HWREG(GPIO_PORTF_BASE + GPIO_O_PUR) | (1<<1);
    HWREG(GPIO_PORTF_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY;
    HWREG(GPIO_PORTF_BASE + GPIO_O_CR) = (1<<0);
		GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1);
		
		//Initialize PF2 for right fan ESC Control
		GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_2);
	
		//Initialize PF3 for right fan direction relay
		GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_3);
	
		//Initialize PF4 for Power Relay
		GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_4);
}


/*
 *	This function is the ISR for the UART1 module
 */
void DOG_UART1_ISR(){
		// check if UART1 interrupt flag for receiver is set
		//if ((HWREG(UART1_BASE + UART_O_RIS) & UART_RIS_RXRIS) > 0){
		if (HWREG(UART1_BASE+UART_O_MIS)& UART_MIS_RXMIS){
			// clear RX interrupt flag
			HWREG(UART1_BASE + UART_O_ICR) = UART_ICR_RXIC;
			uint8_t Received_byte = HWREG(UART1_BASE + UART_O_DR);
			
			// RX State Machine in ISR
			DogRXState_t NextRXState = CurrentRXState;
			switch (CurrentRXState){
				case WaitFor7E:
						// if receive incoming start byte
						if(Received_byte == START_BYTE){
								//#ifdef DEBUGGING_EVENT
								//printf("Received start bytes: %.2x\r\n", Received_byte);
								//#endif
								// first clear current data packet
								ClearRXDataPacket();
								// reset Checksums, received index to 0
								RX_CheckSum = 0;
								Received_RX_CheckSum = 0;
								rx_index = 0;
								// set next state to wait for msb
								NextRXState = WaitForMSBLen;
						}
						// if receive byte other than 0x7E
						else{
								//#ifdef DEBUGGING_EVENT
								//printf("Received invalid start bytes: %.2x\r\n", Received_byte);
								//#endif
						}
						break;
						
				// if current state is wait for MSB
				case WaitForMSBLen:
							// save msb length of the received packet
							RX_Message_MSB_Len = Received_byte;
							// set next state to wait for lsb
							NextRXState = WaitForLSBLen;
						break;
					
				// if current state is wait for LSB
				case WaitForLSBLen:
							// save lsb length of the received packet
							RX_Message_LSB_Len = Received_byte;
							// form data packet length from the received msb and lsb
							RX_DataPacket_Length = (RX_Message_MSB_Len << 8) + RX_Message_LSB_Len;
							// set next state to get data packet
							NextRXState = GetDataPacket;
						break;
						
				// if current state is wait for get data packet
				case GetDataPacket:
							// put received data into DataPacket array
							RX_DataPacket[rx_index] = Received_byte;
							// add the data up to check sum
							RX_CheckSum += Received_byte;
							// increment the received index
							rx_index++;
							// compare index to the received index, if equal go to check sum state
							if(rx_index == RX_DataPacket_Length){
								// compute final check sum by subtracting from 0xFF
								RX_CheckSum = CHECKSUM_OFFSET - RX_CheckSum;
								// set next state to get check sum
								NextRXState = GetCheckSum;
							}
						break;

				// if current state is get check sum
				case GetCheckSum:
						// set check sum to the byte received
						Received_RX_CheckSum = Received_byte;
						// check if it equal to our calculated check sum
						if(Received_RX_CheckSum == RX_CheckSum){
							CopyToBuffer();
							// post received byte to FARMER_RX service
							ES_Event ThisEvent;
							ThisEvent.EventType = VALID_PACKET_RECEIVED;
							ThisEvent.EventParam = RX_DataPacket_Length;
							PostDogRX(ThisEvent);
							
							//#ifdef DEBUGGING_EVENT
							//printf("Received Good Checksum. Post VALID_PACKET_RECEIVED event with length %d\r\n", RX_DataPacket_Length);
							//#endif
						}else{
							//#ifdef DEBUGGING_EVENT
							//printf("Received Bad Checksum. Did not post VALID_PACKET_RECEIVED event\r\n");
							//#endif
						}
						// set next state to wait for start delimiter bytes
						NextRXState = WaitFor7E;
						break;
				
				default:
						break;
			}
			CurrentRXState = NextRXState;
			
		}
	
		// check if UART1 interrupt flag for transmitter is set
		else if(HWREG(UART1_BASE+UART_O_MIS)& UART_MIS_TXMIS){
			
			// write data to UART but first wait till FIFO empty
			if((HWREG(UART1_BASE + UART_O_FR) & UART_FR_TXFE) == UART_FR_TXFE){
					// clear TX interrupt flag	
					HWREG(UART1_BASE + UART_O_ICR) = UART_ICR_TXIC;
					// write data to UART
					WriteToUART();
			}
			// check number of byte remaining
			if(byte_remaining == 0){
				// if byte remaining is zero, disable interrupt on TX
				HWREG(UART1_BASE + UART_O_IM) &= ~UART_IM_TXIM;
				#ifdef DEBUGGING_EVENT
				printf("A Packet has been sent\r\n");
				#endif
			}
		}
}

/*
 *	This function clears data packet ocntent
 */
static void ClearRXDataPacket(){
	for(int i = 0; i < LARGEST_FRAME_DATA; i++){
		// set all index of data packet to 0
		RX_DataPacket[i] = 0;
	}
}

static void ClearRXBuffer(){
	for(int i = 0; i < LARGEST_FRAME_DATA; i++){
		// set all index of data packet to 0
		RX_Buffer_Array[i] = 0;
	}
}

static void CopyToBuffer(){
	// ClearRXBuffer();
	for(int i = 0; i < LARGEST_FRAME_DATA; i++){
		// set all index of data packet to 0
		RX_Buffer_Array[i] = RX_DataPacket[i];
	}
}

uint8_t GetRXBuffer(int index){
	return RX_Buffer_Array[index];
}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

