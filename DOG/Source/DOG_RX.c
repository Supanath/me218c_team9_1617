/****************************************************************************
Definition for debugging
 ***************************************************************************/
#define DEBUGGING_STATE 			// print out farmer's current state
#define DEBUGGING_EVENT 			// print out current event
#define DEBUGGING_ENCRYPTION_KEY
#define DEBUG_RECEIVE_PACKET		// print out received packet

/*Todo
1. Update read Dog ID function
*/

/****************************************************************************
 Module
   DOG_RX.c

 Description
   This is the receiver service for DOG which receive message from the farmer

****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for the framework and this service
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "DOG_RX.h"
#include "DOG_TX.h"
#include "UART_Init.h"
#include "XBEE_DEFS.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"	// Define PART_TM4C123GH6PM in project
#include "driverlib/gpio.h"
#include "ES_ShortTimer.h"
#include "inc/hw_types.h"
#include "inc/hw_uart.h"
#include "inc/hw_nvic.h"
#include "inc/hw_timer.h"
#include "AnalogRead.h"
/*----------------------------- Module Defines ----------------------------*/
// these times assume a 1.000mS/tick timing
#define ONE_SEC 1000
#define HALF_SEC (ONE_SEC/2)
#define TWO_SEC (ONE_SEC*2)
#define FIVE_SEC (ONE_SEC*5)
#define LARGEST_FRAME_DATA 50

// packet byte definition
#define CHECKSUM_OFFSET			0xFF
#define	START_BYTE 0x7E
#define API_TX_ID 0x01
#define OPTION_BYTE 0x00
#define	MSB_LEN 0x00
#define MAX_XBEE_PACKET_SIZE 9
#define API_RX_ID				0x81
#define API_TX_STATUS_ID		0x89
#define	ACK_STATUS				0
#define NACK_STATUS				1
#define AD_CHANNEL_NUM		1
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

static void ClearDataPacket(void);
static void AnalyzeRXPacket(void);
static void ClearEncryption(void);
static void PrintOutReceivedPacket(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static uint8_t DataPacket_Length;
static uint8_t XbeePacket_Length;
static uint8_t CheckSum = 0x00;
static uint8_t Received_CheckSum = 0x00;
static uint8_t API_id = 0x00;
static uint8_t frame_id = 0x00;
static uint8_t tx_status = 0x00;
static uint8_t byte_remaining = 0x00;
static uint8_t RX_Message_MSB_Len = 0;
static uint8_t RX_Message_LSB_Len = 0;
static uint8_t Source_Address_MSB = 0x00;
static uint8_t Source_Address_LSB = 0x00;
static uint8_t dog_id = 0x00;
static uint16_t Farmer_Address = 0x0000;
static uint16_t Paired_Farmer_Address = 0x0000;
static uint8_t MSB_RX_ADDRESS = 0x00;
static uint8_t LSB_RX_ADDRESS = 0x00;
static uint8_t encrypt_index = 0x00;
static uint8_t DataPacket[LARGEST_FRAME_DATA]; //define datapacket array
static uint8_t XbeePacket[LARGEST_FRAME_DATA + MAX_XBEE_PACKET_SIZE];
static const int ENCRYPTION_KEY_LEN = 32;
static uint8_t EncryptKey[ENCRYPTION_KEY_LEN];
static int rx_index = 0;
static const int CTRL_LEN = 4;
static uint32_t ControlMessage = 0;
static bool InControlMode = false;
static uint32_t AD_data[4];
static const uint32_t AD_Tolerance_1 = 300;
static const uint32_t AD_Tolerance_2 = 200;
static const uint32_t AD_id_1 = 1360;
static const uint32_t AD_id_2 = 2100;
static const uint32_t AD_id_3 = 2500;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitDogRX

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any 
     other required initialization for this service
****************************************************************************/
bool InitDogRX ( uint8_t Priority )
{
		ES_Event ThisEvent;
		MyPriority = Priority;
	
		/********************************************
		 Initialization Code
		 *******************************************/
	
		// initialize current state to waiting to pair
		DogRXState_t CurrentRXState = WaitFor7E;
		
		// Initialize variable to 0
		DataPacket_Length = 0;
		XbeePacket_Length = 0;
	
		// enable RX interrupt
		HWREG(UART1_BASE + UART_O_IM) |= UART_IM_RXIM;
	
		// reading ID
		ReadID();
		
		printf("Finished initializing Dog RX Service\r\n");

		// post the initial transition event
		ThisEvent.EventType = ES_INIT;
		if (ES_PostToService( MyPriority, ThisEvent) == true)
		{
				return true;
		}else
		{
				return false;
		}
}

/****************************************************************************
 Function
     PostDogRX

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue

****************************************************************************/
bool PostDogRX( ES_Event ThisEvent )
{
  return ES_PostToService( MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunDogRX

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

****************************************************************************/
ES_Event RunDogRX( ES_Event ThisEvent )
{
	ES_Event ReturnEvent;
	ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
	
	if(ThisEvent.EventType == VALID_PACKET_RECEIVED){
			DataPacket_Length = ThisEvent.EventParam;
			//printf("Data packet length is: %d\r\n", DataPacket_Length);
			for(int i = 0; i < DataPacket_Length; i++){
					//printf("Copy: %.2x\r\n", GetRXBuffer(i));
					DataPacket[i] = GetRXBuffer(i);
			}
			PrintOutReceivedPacket();
			AnalyzeRXPacket();
	}

	return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/*
 *	This function print out the received packet content
 */
static void PrintOutReceivedPacket(){
		#ifdef DEBUG_RECEIVE_PACKET
		printf("Received: ");
		for(int i = 0; i < DataPacket_Length; i++){
			printf("%.2x ", DataPacket[i]);
		}
		printf("\r\n");
		#endif
}

/*
 *	This function update dog id from the attached dog tag
 */
void ReadID(){
	ADC_MultiRead(AD_data);
	uint32_t raw_id_value = AD_data[0];
	printf("Raw id reading: %d\r\n", raw_id_value);
	if(raw_id_value < AD_id_1 + AD_Tolerance_1){
		dog_id = 0x01;
	}
	else if(raw_id_value < AD_id_2 + AD_Tolerance_2){
		dog_id = 0x02;
	}
	else{
		dog_id = 0x03;
	}
	printf("Set dog id to %d\r\n", dog_id);
}

/*
 *	This function clears data packet ocntent
 */
static void ClearDataPacket(){
	for(int i = 0; i < LARGEST_FRAME_DATA; i++){
		// set all index of data packet to 0
		DataPacket[i] = 0;
	}
}

/*
 *	This function clears encryption key array
 */
static void ClearEncryption(){
	for(int j = 0; j < ENCRYPTION_KEY_LEN; j++){
		// reset all encryption keys to 0
		EncryptKey[j] = 0;
	}
}

/*
 *	This function reset the encryption key index
 */
void ResetEncryptKeyIndex(){
	// reset encryption key index to 0
	encrypt_index = 0;
}

/*
 *	This function decrypts the data packet
 */
static void DecryptMessage(int message_len){
	printf("Decrypt key: %.2x, index: %d\r\n", EncryptKey[encrypt_index], encrypt_index);
	// go through the data packet and xor by encrypt key index by index
	for(int i = 0; i < message_len; i++){
		// need to add 6 because for received message, there are 6 index
		// of overhead before the real data packet
		DataPacket[i+5] = DataPacket[i+5] ^ EncryptKey[encrypt_index];
		encrypt_index++;
		if(encrypt_index == 32){
			encrypt_index = 0x00;
		}
	}
}

/*
 *	This function analyze the recieved data packet
 */
static void AnalyzeRXPacket(){
	API_id = DataPacket[0];
	// if api id is 0x89, this is transmit status message
	if(API_id == API_TX_STATUS_ID){
		#ifdef DEBUGGING_EVENT
		printf("Received Transmit Status Message\r\n");
		#endif

		frame_id = DataPacket[1];	// set frame id
		tx_status = DataPacket[2];	// set transmit status
		ES_Event ThisEvent;
		// if transmit status is 0, this is ack
		if(tx_status == ACK_STATUS){
			#ifdef DEBUGGING_EVENT
			printf("Received ACK - transmit successful\r\n");
			#endif
			ThisEvent.EventType = ACK_RECEIVED;
			ThisEvent.EventParam = frame_id;
			PostDogTX(ThisEvent);
		}
		// if transmit status is 1, this is nack
		else if(tx_status == NACK_STATUS){
			#ifdef DEBUGGING_EVENT
			printf("Received NACK - transmit failed\r\n");
			#endif
			ThisEvent.EventType = NACK_RECEIVED;
			ThisEvent.EventParam = frame_id;
			PostDogTX(ThisEvent);
		}
	}
	// if api id is 0x81, this is receive data message
	else if(API_id == API_RX_ID){
		// save source address
		Source_Address_MSB = DataPacket[1];
		Source_Address_LSB = DataPacket[2];
		// set farmer address
		Farmer_Address = (Source_Address_MSB << 8) + Source_Address_LSB;
		
		// if rx is not in control mode (message will not be decrypted)
		if(!InControlMode){
			// if data packet type is request to pair
			if(DataPacket[5] == REG_2_PAIR){
				// reading ID
				ReadID();
				#ifdef DEBUGGING_EVENT
				printf("Dog: Received pair request message with id %.2x\r\n", DataPacket[6]);
				#endif
				// check if received dog id is the same as on board one
				if(dog_id == DataPacket[6]){
					// post get valid dog id response message to farmer top SM
					ES_Event ThisEvent;
					ThisEvent.EventType = PAIR_REQUEST_RECEIVED;
					ThisEvent.EventParam = Farmer_Address;
					PostDogTX(ThisEvent);

					#ifdef DEBUGGING_EVENT
					printf("Posting to transmit module to send reply\r\n"); 
					#endif
				}
				else{
					#ifdef DEBUGGING_EVENT
					printf("Received request to pair with wrong id, ignore the message\r\n");
					#endif
				}
			}
			// if data packet is encryption key message
			else if(DataPacket[5] == ENCR_KEY){
				#ifdef DEBUGGING_EVENT
				printf("Dog: Received encryption key packet\r\n");
				#endif

				// update encryption key
				for(int j = 0; j < ENCRYPTION_KEY_LEN; j++){
					EncryptKey[j] = DataPacket[j + 6];
				}
				
				#ifdef DEBUGGING_ENCRYPTION_KEY
				printf("Encryption key:\r\n");
				for(int j = 0; j < ENCRYPTION_KEY_LEN; j++){
					printf("%.2x ", EncryptKey[j]);
				}
				printf("\r\n");
				#endif
				
				// Save Farmer Address
				Paired_Farmer_Address = Farmer_Address;
				
				// post encryption key received event
				ES_Event ThisEvent;
				ThisEvent.EventType = ENCRYPTION_KEY_RECEIVED;
				ThisEvent.EventParam = Farmer_Address;
				PostDogTX(ThisEvent);
			}
			else{
				#ifdef DEBUGGING_EVENT
				printf("Unknown command received\r\n");
				#endif
			}
		}

		// else if rx is in control command, we need to decrypt data packet
		else{
			if(Farmer_Address == Paired_Farmer_Address){		
				// first decrypt the data packet
				DecryptMessage(CTRL_LEN);
				// if the decrypted data header is 0x04
				if(DataPacket[5] == CTRL){
					#ifdef DEBUGGING_EVENT
					printf("Dog: Received control message\r\n");
					#endif

					// post get valid dog id response message to farmer top SM
					ES_Event ThisEvent;
					ThisEvent.EventType = CTRL_COMMAND_RECEIVED;
					ControlMessage = DataPacket[5] << 24; // reset control message
					ControlMessage += DataPacket[6] << 16;
					ControlMessage += DataPacket[7] << 8;
					ControlMessage += DataPacket[8];

					#ifdef DEBUGGING_ENCRYPTION_KEY
					printf("CTRL: %.8x\r\n", ControlMessage);
					#endif

					ThisEvent.EventParam = ControlMessage;
					PostDogTX(ThisEvent);
					#ifdef DEBUGGING_EVENT
					printf("Posting control command received to Dog TX\r\n"); 
					#endif
				}
				// if received header is not 0x04
				else {
					#ifdef DEBUGGING_EVENT
					printf("Dog: Received invalid control message\r\n");
					#endif

					// post get valid dog id response message to farmer top SM
					ES_Event ThisEvent;
					ThisEvent.EventType = INVALID_CTRL_COMMAND_RECEIVED;
					PostDogTX(ThisEvent);
				}
			}
			else{
				printf("Received message from other unpaired farmer: %.4x, ignore it\r\n", Farmer_Address);
			}
		}
	}
	
}

/*
 *	This function updates control mode according to the input
 */
void SetInControlMode(bool input){
		InControlMode = input;
}

uint32_t GetControlMessage(){
		return ControlMessage;
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

