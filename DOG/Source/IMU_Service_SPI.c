/****************************************************************************
Definition for debugging
 ***************************************************************************/
//#define DEBUGGING_IMU_READ 			// print out IMU read valud

/****************************************************************************
 Module
   IMU_Service_SPI.c

****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "IMU_Service_SPI.h"

// some includes used in the SPI.c from Lab 8
#include "ES_DeferRecall.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"	// Define PART_TM4C123GH6PM in project
#include "driverlib/gpio.h"
//#include "ES_ShortTimer.h"
#include "inc/hw_timer.h"
#include "inc/hw_ssi.h"
#include "inc/hw_nvic.h"

//some testing constants
#define TEST_LOC_STAGE

/*----------------------------- Module Defines ----------------------------*/
#define READ_IMU_PERIOD  5//50 Hz, we only send by 5Hz anyway
#define BOOTING_PERIOD 200 //a time for the IMU to finish boot procedure
#define POWER_ON_PERIOD 500 //500ms for it to start

//set up sequence register addresses
#define CTRL9_XL 0x18
#define CTRL1_XL 0x10
#define INT1_CTRL 0x0D
#define CTRL10_C 0x19
#define CTRL2_G 0x11
#define INT1_CTRL 0x0D
#define CTRL7_G 0x16
//raw data register addresses
#define GYRO_X_L 0x22
#define GYRO_X_H 0X23
#define GYRO_Y_L 0x24
#define GYRO_Y_H 0X25
#define GYRO_Z_L 0x26
#define GYRO_Z_H 0X27
#define ACCEL_X_L 0x28
#define ACCEL_X_H 0x29
#define ACCEL_Y_L 0x2A
#define ACCEL_Y_H 0x2B
#define ACCEL_Z_L 0x2C
#define ACCEL_Z_H 0x2D
#define READ_IMU_MASK 0x80 // "or" with the above register addresses
#define WRITE_IMU_MASK 0x00 
#define WHO_AM_I_MASK 0x0F
#define CTRL3_C 0x12


// these times assume a 1.000mS/tick timing
#define TEN_MSEC 10
#define ONE_SEC 976
#define HALF_SEC (ONE_SEC /2)
#define TWO_SEC (ONE_SEC *2)
#define FIVE_SEC (ONE_SEC *5)
#define BitsPerNibble 4
#define CPSDVSR_PRESCALER 128
#define SCR 60 //date rate should be 1/60 of the Lab 8's data rate, due to Tcy


	//the input would be in ticks, 4*10^7 ticks in one sec, period table is in micro seconds,
	//4*10^7 ticks in one sec is 4*10^7 ticks in 10^6 micro
#define TicksToMicroSecDivisor 40

/*---------------------------- Module Functions ---------------------------*/
static void enable_SPI_Interupt(void);
/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, though if the top level state machine
// is just a single state container for orthogonal regions, you could get
// away without it
static IMU_Service_SPI_State_t CurrentState;
// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

//our data consists 2 bytes for each sensor data, with 6 in total for gyro and accel
static uint8_t data1;
static uint8_t data2;
static uint8_t data3;

// sacrificial used to read when doing config, we still want to read out to clear data buffer
static uint8_t dataConfigOne;
static uint8_t dataConfigTwo;

//some flags indicating current states
uint8_t FlagDoingConfig=0;
uint8_t FlagDoingData=0;

//constructed display variables
static int16_t Z_Accel=0;

#define NUM_OF_DATA 6
// [0] x accel [1] y accel [2] z accel [3] x gyro [4]  y gyro  [5] z gyro
static int16_t IMUData[NUM_OF_DATA] = {0}; //just initialize, all elements are zero
static uint8_t IMUData_Counter=0; //used to populate the data array

static const uint8_t IMU_READ_DATA_COMMAND[NUM_OF_DATA] = {
	(READ_IMU_MASK | ACCEL_X_L),
	(READ_IMU_MASK | ACCEL_Y_L),
	(READ_IMU_MASK | ACCEL_Z_L),
	(READ_IMU_MASK | GYRO_X_L),
	(READ_IMU_MASK | GYRO_Y_L),
	(READ_IMU_MASK | GYRO_Z_L)
};



static uint8_t currInitStep=0;
#define NUM_INIT_STEPS 6
static const uint16_t InitAddress[NUM_INIT_STEPS] = {
  (CTRL9_XL), 
  (CTRL1_XL),  
 // (INT1_CTRL)
	CTRL3_C,  //so it's a block of data, making sure high byte and low byte correspond to a same time
  (CTRL10_C),  
    CTRL2_G,
	CTRL7_G
//    INT1_CTRL
};

static const uint16_t InitValue[NUM_INIT_STEPS] = {
  (0x38), 
  (0x60), 
 // (0x01)
	0x44,  
  0x38, 
  0x60,
  0x60	
//    0x02
};

//initialize a event to carry information around
//static ES_Event EventToPost;
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitIMU_Service_SPI

 Parameters
     uint8_t : the priorty of this service

 Returns
     boolean, False if error in initialization, True otherwise

 Description
     Saves away the priority,  and starts
     the top level state machine

****************************************************************************/
bool InitIMU_Service_SPI ( uint8_t Priority )
{
	TERMIO_Init();
  //ES_Event ThisEvent;

  MyPriority = Priority;  // save our priority
	CurrentState=POWER_ON_WAITING;

	SPI_Init();
	//kick off the timer to read the IMU
	ES_Timer_InitTimer(READ_IMU_TIMER,POWER_ON_PERIOD);
	FlagDoingConfig=1;
	FlagDoingData=0;
  return true;
}

/****************************************************************************
 Function
     PostIMU_Service_SPI

 Parameters
     ES_Event ThisEvent , the event to post to the queue

 Returns
     boolean False if the post operation failed, True otherwise

 Description
     Posts an event to this state machine's queue

****************************************************************************/
bool PostIMU_Service_SPI( ES_Event ThisEvent )
{
  return ES_PostToService( MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunIMU_Service_SPI

 Parameters
   ES_Event: the event to process

 Returns
   ES_Event: an event to return

 Description
   the run function for the top level state machine 
 Notes
   uses nested switch/case to implement the machine.

****************************************************************************/
ES_Event RunIMU_Service_SPI( ES_Event CurrentEvent )
{
  
   ES_Event ReturnEvent = { ES_NO_EVENT, 0 }; // assume no error


    switch ( CurrentState )
   {
			case POWER_ON_WAITING:
				if(CurrentEvent.EventType== ES_TIMEOUT){ //If event is GET_STATUS timer timeout
								 if(CurrentEvent.EventParam==READ_IMU_TIMER){
									 CurrentState=BOOTING_IMU;
									 //give some time until the next step
										ES_Timer_InitTimer(READ_IMU_TIMER,BOOTING_PERIOD);
								 }
							 }
				break;
		 
      case BOOTING_IMU:
				if(CurrentEvent.EventType== ES_TIMEOUT){ //If event is GET_STATUS timer timeout
								 if(CurrentEvent.EventParam==READ_IMU_TIMER){
									 
									 if (currInitStep<NUM_INIT_STEPS){
										 printf("take a step \n\r");
										 FlagDoingConfig=1;
										 //do this to force the cs high after each step
										 SPI_Write(InitAddress[currInitStep]);
										 SPI_Write(InitValue[currInitStep]);
										 enable_SPI_Interupt();
										 //increment steps
										 currInitStep=currInitStep+1;
										 //give some time until the next step
										 ES_Timer_InitTimer(READ_IMU_TIMER,BOOTING_PERIOD);
									 }
									 if (currInitStep==NUM_INIT_STEPS){
										 printf("finished config\n\r");
										 //finished configuring
										 FlagDoingConfig=0;
										 FlagDoingData=1;
										 //clear the counter
										 currInitStep=0;
										 //state transition
										 CurrentState=COMMANDING_IMU; //booting is finished, move to commanding
										 //kick off timer for real sensor reading
									 ES_Timer_InitTimer(READ_IMU_TIMER,READ_IMU_PERIOD); //kick off timer
									 }
									 						 
								 }
							 }
				break;
									 
			case COMMANDING_IMU:
					if(CurrentEvent.EventType== ES_TIMEOUT){ //If event is GET_STATUS timer timeout
								 if(CurrentEvent.EventParam==READ_IMU_TIMER){
										if (FlagDoingData==1){ //that means we are doing data, and the stuff in SPI are actually data, not init stuff
											SPI_Write( IMU_READ_DATA_COMMAND[IMUData_Counter]);
											SPI_Write(0x00);
											SPI_Write(0x00); //by default consecutive reads would increment the address and read that
											//enable the EOT interrupt
											enable_SPI_Interupt();
										}
                  
									 //state transition
									 CurrentState=RECEIVING_FROM_IMU;

                  // optionally, consume or re-map this event for the upper
                  // level state machine
                  ReturnEvent.EventType = ES_NO_EVENT;
								 }
							 }
                  break;

      // repeat state pattern as required for other states

     case RECEIVING_FROM_IMU :       // If current state is state on+
               	if(CurrentEvent.EventType == ES_EOT){
//											printf("Byte 1 : 0x%02x\r\n", data1);//put a lot of spaces around this so I can find it easily
//											printf("Byte 2 : 0x%02x\r\n", data2);
//									printf("Byte 3 : 0x%02x\r\n", data3);
//									printf("Byte 2 : %d\r\n", data2);
											int16_t tempInt16Buffer=0;
											tempInt16Buffer = ( ((tempInt16Buffer | ((int16_t) data3))<<8)|( (int16_t) data2));
										IMUData[IMUData_Counter]=tempInt16Buffer;
//									if(IMUData_Counter==2){ //only print out one to verify
//									    printf("Value: %d \n\r", IMUData[IMUData_Counter]);
//									}
									
									IMUData_Counter=IMUData_Counter+1;
				
									if (IMUData_Counter==NUM_OF_DATA){
										IMUData_Counter=0; //max out, revert back to zero
										#ifdef DEBUGGING_IMU_READ 
										printf(" %d, %d,  %d,  %d,  %d,  %d\n\r", IMUData[0],IMUData[1],IMUData[2],IMUData[3],IMUData[4],IMUData[5]); //print out all data
										#endif
									}
                  //start the timer for the next game status query
                  ES_Timer_InitTimer(READ_IMU_TIMER,READ_IMU_PERIOD);
									//state transition
									 CurrentState=COMMANDING_IMU;
								}
									break;
								
		 default:
			 break;
				 
	 }	 
        return (ReturnEvent);
}

/********************************************SPI FUNCTIONS********************************************/

void SPI_Init(void){
	// Enable the clock to the GPIO Port (we are going to use Port D)
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
	// Enable clock to SSI - set to SSI Module 3
	HWREG(SYSCTL_RCGCSSI) |= SYSCTL_RCGCSSI_R3;
	// Wait for GPIO Port to be ready by killing a few cycles
	while((HWREG(SYSCTL_RCGCGPIO) & SYSCTL_RCGCGPIO_R3) != SYSCTL_RCGCGPIO_R3){}
	// Program the GPIO to use the alternate functions on the SSI pins PD0,1,2,3
	HWREG(GPIO_PORTD_BASE + GPIO_O_AFSEL) = (HWREG(GPIO_PORTD_BASE + GPIO_O_AFSEL) 
		& 0xfffffff0) | (BIT0HI | BIT1HI| BIT2HI| BIT3HI);
	// Set Mux position in GPIOPCTL to select the SSI use of the pins
	HWREG(GPIO_PORTD_BASE+GPIO_O_PCTL) =
		(HWREG(GPIO_PORTD_BASE+GPIO_O_PCTL) & 0xffff0000) + (1<<(3*BitsPerNibble)) 
		+ (1<<(2*BitsPerNibble)) + (1<<(1*BitsPerNibble)) + 1;
	// Program the port lines for digital I/O
	HWREG(GPIO_PORTD_BASE+GPIO_O_DEN) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI);
	// Program the required data directions on the port line
	HWREG(GPIO_PORTD_BASE+GPIO_O_DIR) &= (~BIT2HI); //PD2 is the input (receiver line)
	HWREG(GPIO_PORTD_BASE+GPIO_O_DIR) |= (BIT0HI | BIT1HI | BIT3HI);
	// If using SPI mode 3, program the pull-up on the clock line
	HWREG(GPIO_PORTD_BASE + GPIO_O_PUR) |= BIT0HI;
	// Wait for the SSI0 to be ready
	while((HWREG(SYSCTL_RCGCSSI) & SYSCTL_RCGCSSI_R3) != SYSCTL_RCGCSSI_R3){}
	// Make sure that the SSI is disabled before programming mode bits
	HWREG(SSI3_BASE + SSI_O_CR1) = HWREG(SSI3_BASE + SSI_O_CR1) & ~SSI_CR1_SSE;
	// Select Master mode and TXRES indicating EOT
	HWREG(SSI3_BASE + SSI_O_CR1) &= ~SSI_CR1_MS; //Set to 0 for master
	HWREG(SSI3_BASE + SSI_O_CR1) |=	SSI_CR1_EOT; //Set EOT to 1 (for interupt)
	// Configure the SSI clock source to the system clock
	HWREG(SSI3_BASE + SSI_O_CC) = (HWREG(SSI3_BASE + SSI_O_CC) & ~SSI_CC_CS_M) | SSI_CC_CS_SYSPLL;
	// Configure the clock pre-scaler: here we want CPSDVSR = , 1+SCR = 
	HWREG(SSI3_BASE + SSI_O_CPSR) = (HWREG(SSI3_BASE + SSI_O_CPSR) & 
		~SSI_CPSR_CPSDVSR_M) | CPSDVSR_PRESCALER; //set CPSDVSR = 80
	// Configure clock rate (SCR) - 0, phase (SPH)- 1 and 
	// polarity (SPO)- 1 ,mode (FRF) - freescale(0) and datasize (DSS) - 8 bit
	HWREG(SSI3_BASE + SSI_O_CR0) = (HWREG(SSI3_BASE + SSI_O_CR0) & 0xffff0000) 
		| ((SCR << 8)| SSI_CR0_SPH | SSI_CR0_SPO | SSI_CR0_DSS_8 | SSI_CR0_FRF_MOTO);
	// Locally Enable Interrupts (TXIM in SSIIM)
	enable_SPI_Interupt();
	// Enable SSI
	HWREG(SSI3_BASE + SSI_O_CR1) = (HWREG(SSI3_BASE + SSI_O_CR1) & ~SSI_CR1_SSE) | SSI_CR1_SSE;
	// Globally enable interupts
	__enable_irq();
	// Enable the NVIC interrupt for the SSI when starting to transmit
	// enable SSI3 interrupt in the NVIC, it is interrupt number 58 so appears in EN2 at bit 0
	HWREG(NVIC_EN1) = BIT26HI;
	
	//make sure we disable loopback mode
	HWREG(SSI3_BASE + SSI_O_CR1) &= (~SSI_CR1_LBM); //DISABLE LOOP BACK MODE FOR SURE
}



// We enable or disable interupt by setting or clearing TXIM
static void enable_SPI_Interupt(void){
  HWREG(SSI3_BASE+SSI_O_IM) |= SSI_IM_TXIM;
}

static void disable_SPI_Interupt(void){
  HWREG(SSI3_BASE+SSI_O_IM) &= ~SSI_IM_TXIM;
}

void SPI_Interupt_Response(void){
	
  disable_SPI_Interupt(); //disable the interupt
  
	//consecutive read try
	data1=SPI_Read(); //expect data 1 to be 0x00
	data2=SPI_Read();
	data3=SPI_Read();
	
	if(FlagDoingData==1){
	ES_Event new_event; //finish 5 bytes sequence, post EOT of 5 bytes
  new_event.EventType = ES_EOT;
  PostIMU_Service_SPI(new_event); //basically assemble and print
	}

}

// Set the data to SPI register
void SPI_Write(uint8_t data){
  HWREG(SSI3_BASE+SSI_O_DR) = data;
	enable_SPI_Interupt(); //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<SHOULD TEST WHERE THE BEST PLACE IT IS TO DO THIS
}

// Read the data from SPI register
uint8_t SPI_Read(void){
	uint8_t dataHolder;
  dataHolder = HWREG(SSI3_BASE+SSI_O_DR);
  return dataHolder;
}

uint16_t GetIMUData(int index){
	return IMUData[index];
}