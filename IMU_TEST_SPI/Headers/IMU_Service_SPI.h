/****************************************************************************
 Template header file for Hierarchical Sate Machines AKA StateCharts
 02/08/12 adjsutments for use with the Events and Services Framework Gen2
 3/17/09  Fixed prototpyes to use Event_t
 ****************************************************************************/

#ifndef IMU_SERVICE_SPI_H
#define IMU_SERVICE_SPI_H


// typedefs for the states
// State definitions for use with the query function
typedef enum { BOOTING_IMU,COMMANDING_IMU, RECEIVING_FROM_IMU } IMU_Service_SPI_State_t ;


// Public Function Prototypes

ES_Event RunIMU_Service_SPI( ES_Event CurrentEvent );
bool PostIMU_Service_SPI( ES_Event ThisEvent );
bool InitIMU_Service_SPI ( uint8_t Priority );
void IMU_SPI_Init(void);


//SPI function
void SPI_Init(void);
uint8_t SPI_Read(void);
void SPI_Write(uint8_t data);
void SPI_Interupt_Response(void);


#endif 

