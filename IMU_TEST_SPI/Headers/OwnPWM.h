#ifndef OWNPWM_H
#define OWNPWM_H

#include <stdint.h>

void InitOwnPWM( void );

void SetZeroDCA(void);
void SetZeroDCB(void);
void RestoreDCA(void);
void RestoreDCB(void);
void SetHundredDCA(void);
void SetHundredDCB(void);
void OwnPWM_SetFreq(uint32_t freq);
void OwnPWM_SetDutyA(uint32_t duty);
void OwnPWM_SetDutyB(uint32_t duty);
#endif
