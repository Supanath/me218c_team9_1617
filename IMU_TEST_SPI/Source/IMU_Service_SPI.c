/****************************************************************************
 Module
   TopHSMTemplate.c

 Revision
   2.0.1

 Description
   This is a template for the top level Hierarchical state machine

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 02/08/12 01:39 jec      converted from MW_MasterMachine.c
 02/06/12 22:02 jec      converted to Gen 2 Events and Services Framework
 02/13/10 11:54 jec      converted During functions to return Event_t
                         so that they match the template
 02/21/07 17:04 jec      converted to pass Event_t to Start...()
 02/20/07 21:37 jec      converted to use enumerated type for events
 02/21/05 15:03 jec      Began Coding
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "IMU_Service_SPI.h"

// some includes used in the SPI.c from Lab 8
#include "ES_DeferRecall.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"	// Define PART_TM4C123GH6PM in project
#include "driverlib/gpio.h"
//#include "ES_ShortTimer.h"
#include "inc/hw_timer.h"
#include "inc/hw_ssi.h"
#include "inc/hw_nvic.h"

//some testing constants
#define TEST_LOC_STAGE

/*----------------------------- Module Defines ----------------------------*/
#define READ_IMU_PERIOD 100 //10 Hz, we only send by 5Hz anyway
#define BOOTING_PERIOD 100 //a time for the IMU to finish boot procedure

//set up sequence register addresses
#define CTRL9_XL 0x18
#define CTRL1_XL 0x10
#define INT1_CTRL 0x0D
#define CTRL10_C 0x19
#define CTRL2_G 0x11
#define INT1_CTRL 0x0D
//raw data register addresses
#define GYRO_X_L 0x22
#define GYRO_X_H 0X23
#define GYRO_Y_L 0x24
#define GYRO_Y_H 0X25
#define GYRO_Z_L 0x26
#define GYRO_Z_H 0X27
#define ACCEL_X_L 0x28
#define ACCEL_X_H 0x29
#define ACCEL_Y_L 0x2A
#define ACCEL_Y_H 0x2B
#define ACCEL_Z_L 0x2C
#define ACCEL_Z_H 0x2D
#define READ_IMU_MASK 0x80 // "or" with the above register addresses
#define WRITE_IMU_MASK 0x00 


// these times assume a 1.000mS/tick timing
#define TEN_MSEC 10
#define ONE_SEC 976
#define HALF_SEC (ONE_SEC /2)
#define TWO_SEC (ONE_SEC *2)
#define FIVE_SEC (ONE_SEC *5)
#define BitsPerNibble 4
#define CPSDVSR_PRESCALER 128
#define SCR 200 //date rate should be 1/200 of the Lab 8's data rate, due to Tcy


	//the input would be in ticks, 4*10^7 ticks in one sec, period table is in micro seconds,
	//4*10^7 ticks in one sec is 4*10^7 ticks in 10^6 micro
#define TicksToMicroSecDivisor 40


/*---------------------------- Module Functions ---------------------------*/
static void enable_SPI_Interupt(void);
/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, though if the top level state machine
// is just a single state container for orthogonal regions, you could get
// away without it
static IMU_Service_SPI_State_t CurrentState;
// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

//our data consists 2 bytes for each sensor data, with 6 in total for gyro and accel
static uint8_t data1;
static uint8_t data2;
static uint8_t data3;
static uint8_t data4;
static uint8_t data5;
static uint8_t data6;
static uint8_t data7;
static uint8_t data8;
static uint8_t data9;
static uint8_t data10;
static uint8_t data11;
static uint8_t data12; 
static uint8_t data13; 



//initialize a event to carry information around
//static ES_Event EventToPost;
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitMasterSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     boolean, False if error in initialization, True otherwise

 Description
     Saves away the priority,  and starts
     the top level state machine
 Notes

 Author
     J. Edward Carryer, 02/06/12, 22:06
****************************************************************************/
bool InitIMU_Service_SPI ( uint8_t Priority )
{
	TERMIO_Init();
  //ES_Event ThisEvent;

  MyPriority = Priority;  // save our priority
	CurrentState=BOOTING_IMU;

	SPI_Init();
	
	IMU_SPI_Init();
	//kick off the timer to read the IMU
	ES_Timer_InitTimer(READ_IMU_TIMER,BOOTING_PERIOD);
  return true;
}

/****************************************************************************
 Function
     PostMasterSM

 Parameters
     ES_Event ThisEvent , the event to post to the queue

 Returns
     boolean False if the post operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostIMU_Service_SPI( ES_Event ThisEvent )
{
  return ES_PostToService( MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunMasterSM

 Parameters
   ES_Event: the event to process

 Returns
   ES_Event: an event to return

 Description
   the run function for the top level state machine 
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 02/06/12, 22:09
****************************************************************************/
ES_Event RunIMU_Service_SPI( ES_Event CurrentEvent )
{
  
   ES_Event ReturnEvent = { ES_NO_EVENT, 0 }; // assume no error


    switch ( CurrentState )
   {
      case BOOTING_IMU:
				if(CurrentEvent.EventType== ES_TIMEOUT){ //If event is GET_STATUS timer timeout
								 if(CurrentEvent.EventParam==READ_IMU_TIMER){
									 CurrentState=COMMANDING_IMU; //booting is finished, move to commanding
									 ES_Timer_InitTimer(READ_IMU_TIMER,READ_IMU_PERIOD); //kick off timer
								 }
							 }
				break;
									 
			case COMMANDING_IMU:
					if(CurrentEvent.EventType== ES_TIMEOUT){ //If event is GET_STATUS timer timeout
								 if(CurrentEvent.EventParam==READ_IMU_TIMER){
									printf("enter sending query\n\r");
									//just load the addresses to the SPI data register and let them shift out
//									SPI_Write( (READ_IMU_MASK|GYRO_X_L) );
//									SPI_Write( (READ_IMU_MASK|GYRO_X_H) );
//									SPI_Write( (READ_IMU_MASK|GYRO_Y_L) );
//									SPI_Write( (READ_IMU_MASK|GYRO_Y_H) );
//									SPI_Write( (READ_IMU_MASK|GYRO_Z_L) );
//									SPI_Write( (READ_IMU_MASK|GYRO_Z_H) );
									SPI_Write( (READ_IMU_MASK|ACCEL_X_L) );
									 SPI_Write( 0x00);
									SPI_Write( (READ_IMU_MASK|ACCEL_X_H) );
									 SPI_Write( 0x00);
									SPI_Write( (READ_IMU_MASK|ACCEL_Y_L) );
									SPI_Write( 0x00);
									SPI_Write( (READ_IMU_MASK|ACCEL_Y_H) );
									 SPI_Write( 0x00);
									SPI_Write( (READ_IMU_MASK|ACCEL_Z_L) );
									 SPI_Write( 0x00);
									SPI_Write( (READ_IMU_MASK|ACCEL_Z_H) );
									 SPI_Write( 0x00);
//									SPI_Write( 0x00); //To account for the offset on SDO
                 
                  //enable the EOT interrupt
                  enable_SPI_Interupt();
                  
									 //state transition
									 CurrentState=RECEIVING_FROM_IMU;

                  // optionally, consume or re-map this event for the upper
                  // level state machine
                  ReturnEvent.EventType = ES_NO_EVENT;
								 }
							 }
                  break;

      // repeat state pattern as required for other states

     case RECEIVING_FROM_IMU :       // If current state is state on
               	if(CurrentEvent.EventType==ES_EOT){
											printf("Byte 1 : 0x%02x\r\n", data1);//put a lot of spaces around this so I can find it easily
											printf("Byte 2 : 0x%02x\r\n", data2);
											printf("Byte 3 : 0x%02x\r\n", data3);
											printf("Byte 4 : 0x%02x\r\n", data4);
											printf("Byte 5 : 0x%02x\r\n\n", data5);
											printf("Byte 6 : 0x%02x\r\n", data6);//put a lot of spaces around this so I can find it easily
											printf("Byte 7 : 0x%02x\r\n", data7);
											printf("Byte 8 : 0x%02x\r\n", data8);
											printf("Byte 9 : 0x%02x\r\n", data9);
											printf("Byte 10 : 0x%02x\r\n\n", data10);
											printf("Byte 11 : 0x%02x\r\n\n", data11);
											printf("Byte 12 : 0x%02x\r\n\n", data12);
//											printf("Byte 13 received: 0x%02x\r\n\n", data13);
                  //start the timer for the next game status query
                  ES_Timer_InitTimer(READ_IMU_TIMER,READ_IMU_PERIOD);
									//state transition
									 CurrentState=COMMANDING_IMU;
								}
									break;
								
		 default:
			 break;
				 
	 }	 
        return (ReturnEvent);
}

/********************************************SPI FUNCTIONS********************************************/

void SPI_Init(void){
	// Enable the clock to the GPIO Port (we are going to use Port D)
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
	// Enable clock to SSI - set to SSI Module 3
	HWREG(SYSCTL_RCGCSSI) |= SYSCTL_RCGCSSI_R3;
	// Wait for GPIO Port to be ready by killing a few cycles
	while((HWREG(SYSCTL_RCGCGPIO) & SYSCTL_RCGCGPIO_R3) != SYSCTL_RCGCGPIO_R3){}
	// Program the GPIO to use the alternate functions on the SSI pins PD0,1,2,3
	HWREG(GPIO_PORTD_BASE + GPIO_O_AFSEL) = (HWREG(GPIO_PORTD_BASE + GPIO_O_AFSEL) 
		& 0xfffffff0) | (BIT0HI | BIT1HI| BIT2HI| BIT3HI);
	// Set Mux position in GPIOPCTL to select the SSI use of the pins
	HWREG(GPIO_PORTD_BASE+GPIO_O_PCTL) =
		(HWREG(GPIO_PORTD_BASE+GPIO_O_PCTL) & 0xffff0000) + (1<<(3*BitsPerNibble)) 
		+ (1<<(2*BitsPerNibble)) + (1<<(1*BitsPerNibble)) + 1;
	// Program the port lines for digital I/O
	HWREG(GPIO_PORTD_BASE+GPIO_O_DEN) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI);
	// Program the required data directions on the port line
	HWREG(GPIO_PORTD_BASE+GPIO_O_DIR) &= (~BIT2HI); //PD2 is the input (receiver line)
	HWREG(GPIO_PORTD_BASE+GPIO_O_DIR) |= (BIT0HI | BIT1HI | BIT3HI);
	// If using SPI mode 3, program the pull-up on the clock line
	HWREG(GPIO_PORTD_BASE + GPIO_O_PUR) |= BIT0HI;
	// Wait for the SSI0 to be ready
	while((HWREG(SYSCTL_RCGCSSI) & SYSCTL_RCGCSSI_R3) != SYSCTL_RCGCSSI_R3){}
	// Make sure that the SSI is disabled before programming mode bits
	HWREG(SSI3_BASE + SSI_O_CR1) = HWREG(SSI3_BASE + SSI_O_CR1) & ~SSI_CR1_SSE;
	// Select Master mode and TXRES indicating EOT
	HWREG(SSI3_BASE + SSI_O_CR1) &= ~SSI_CR1_MS; //Set to 0 for master
	HWREG(SSI3_BASE + SSI_O_CR1) |=	SSI_CR1_EOT; //Set EOT to 1 (for interupt)
	// Configure the SSI clock source to the system clock
	HWREG(SSI3_BASE + SSI_O_CC) = (HWREG(SSI3_BASE + SSI_O_CC) & ~SSI_CC_CS_M) | SSI_CC_CS_SYSPLL;
	// Configure the clock pre-scaler: here we want CPSDVSR = , 1+SCR = 
	HWREG(SSI3_BASE + SSI_O_CPSR) = (HWREG(SSI3_BASE + SSI_O_CPSR) & 
		~SSI_CPSR_CPSDVSR_M) | CPSDVSR_PRESCALER; //set CPSDVSR = 80
	// Configure clock rate (SCR) - 0, phase (SPH)- 1 and 
	// polarity (SPO)- 1 ,mode (FRF) - freescale(0) and datasize (DSS) - 8 bit
	HWREG(SSI3_BASE + SSI_O_CR0) = (HWREG(SSI3_BASE + SSI_O_CR0) & 0xffff0000) 
		| ((SCR << 8)| SSI_CR0_SPH | SSI_CR0_SPO | SSI_CR0_DSS_8 | SSI_CR0_FRF_MOTO);
	// Locally Enable Interrupts (TXIM in SSIIM)
	enable_SPI_Interupt();
	// Enable SSI
	HWREG(SSI3_BASE + SSI_O_CR1) = (HWREG(SSI3_BASE + SSI_O_CR1) & ~SSI_CR1_SSE) | SSI_CR1_SSE;
	// Globally enable interupts
	__enable_irq();
	// Enable the NVIC interrupt for the SSI when starting to transmit
	// enable SSI3 interrupt in the NVIC, it is interrupt number 58 so appears in EN2 at bit 0
	HWREG(NVIC_EN1) = BIT26HI;
	
	//make sure we disable loopback mode
	HWREG(SSI3_BASE + SSI_O_CR1) &= (~SSI_CR1_LBM); //DISABLE LOOP BACK MODE FOR SURE
}
// We enable or disable interupt by setting or clearing TXIM
static void enable_SPI_Interupt(void){
  HWREG(SSI3_BASE+SSI_O_IM) |= SSI_IM_TXIM;
}

static void disable_SPI_Interupt(void){
  HWREG(SSI3_BASE+SSI_O_IM) &= ~SSI_IM_TXIM;
}

void SPI_Interupt_Response(void){
	
  disable_SPI_Interupt(); //disable the interupt
  
	//consecutive read try
	data1=SPI_Read(); //expect data 1 to be 0x00
	//printf("Passed data1\n\r");
	data2=SPI_Read();
	data3=SPI_Read();
	data4=SPI_Read();
	data5=SPI_Read();
	data6=SPI_Read();
	data7=SPI_Read();
	data8=SPI_Read();
	data9=SPI_Read();
	data10=SPI_Read();
	data11=SPI_Read();
	data12=SPI_Read();
//	data13=SPI_Read();
	
	ES_Event new_event; //finish 5 bytes sequence, post EOT of 5 bytes
  new_event.EventType = ES_EOT;
  PostIMU_Service_SPI(new_event);
	

}

// Set the data to SPI register
void SPI_Write(uint8_t data){
  HWREG(SSI3_BASE+SSI_O_DR) = data;
}

// Read the data from SPI register
uint8_t SPI_Read(void){
	uint8_t dataHolder;
  dataHolder = HWREG(SSI3_BASE+SSI_O_DR);
  return dataHolder;
}

/******************************Other functions***************/
void IMU_SPI_Init(void){
	printf("IMU Init fcn\n\r");
	SPI_Write(WRITE_IMU_MASK|CTRL9_XL);
	SPI_Write(0x38);
	SPI_Write(WRITE_IMU_MASK|CTRL1_XL);
	SPI_Write(0x60);
	SPI_Write(WRITE_IMU_MASK|CTRL10_C);
	SPI_Write(0x38);
	SPI_Write(WRITE_IMU_MASK|CTRL2_G);
	SPI_Write(0x60);
}