/****************************************************************************
Definition for debugging
 ***************************************************************************/
#define DEBUGGING_STATE 			// print out farmer's current state
#define DEBUGGING_EVENT 			// print out current event
//#define DEBUG_RECEIVE_PACKET		// print out received packet

/****************************************************************************
 Module
   Farmer_RX.c

 Description
   This is the receiver service for FARMER which acts as a controller of the DOG

****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for the framework and this service
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "Farmer_RX.h"
#include "FarmerTopSM.h"
#include "UART_Init.h"
#include "XBEE_DEFS.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"	// Define PART_TM4C123GH6PM in project
#include "driverlib/gpio.h"
#include "ES_ShortTimer.h"
#include "inc/hw_types.h"
#include "inc/hw_uart.h"
#include "inc/hw_nvic.h"
#include "inc/hw_timer.h"

/*----------------------------- Module Defines ----------------------------*/
// these times assume a 1.000mS/tick timing
#define ONE_SEC 				1000
#define HALF_SEC 				(ONE_SEC/2)
#define TWO_SEC 				(ONE_SEC*2)
#define FIVE_SEC 				(ONE_SEC*5)

// packet byte definition
#define	START_BYTE 						0x7E
#define API_TX_ID 						0x01
#define OPTION_BYTE 					0x00
#define	MSB_LEN 							0x00
#define CHECKSUM_OFFSET				0xFF
#define MAX_XBEE_PACKET_SIZE 	9
#define STATUS_LEN						12
#define LARGEST_FRAME_DATA 		50
#define API_RX_ID							0x81
#define API_TX_STATUS_ID			0x89
#define	ACK_STATUS						0
#define NACK_STATUS						1
#define IMU_LEN								6
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static void ClearDataPacket(void);
static void AnalyzeRXPacket(void);
static void PrintOutReceivedPacket(void);
static void PrintOutStatusMessage(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static uint8_t DataPacket[LARGEST_FRAME_DATA]; //define datapacket array
static uint8_t XbeePacket[LARGEST_FRAME_DATA + MAX_XBEE_PACKET_SIZE];
static uint8_t IMU_Status[STATUS_LEN];
static int16_t IMU_Value[IMU_LEN];
static uint8_t DataPacket_Length;
static uint8_t XbeePacket_Length;
static uint8_t CheckSum = 0x00;
static uint8_t Received_CheckSum = 0x00;
static uint8_t API_id = 0x00;
static uint8_t frame_id = 0x00;
static uint8_t tx_status = 0x00;
static uint8_t byte_remaining = 0x00;
static int rx_index = 0;
static uint8_t RX_Message_MSB_Len = 0;
static uint8_t RX_Message_LSB_Len = 0;
static uint8_t Source_Address_MSB = 0x00;
static uint8_t Source_Address_LSB = 0x00;
static uint16_t Dog_Address = 0x0000;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitFarmerRX

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any 
     other required initialization for this service

****************************************************************************/
bool InitFarmerRX ( uint8_t Priority )
{
		ES_Event ThisEvent;
		MyPriority = Priority;
	
		/********************************************
		 Initialization Code
		 *******************************************/
		
		// Initialize variable to 0
		DataPacket_Length = 0;
		XbeePacket_Length = 0;
	
		// enable RX interrupt
		HWREG(UART1_BASE + UART_O_IM) |= UART_IM_RXIM;
		
		// print out initializing completion
		printf("Finished initializing Farmer RX Service\r\n");

		// post the initial transition event
		ThisEvent.EventType = ES_INIT;
		if (ES_PostToService( MyPriority, ThisEvent) == true)
		{
				return true;
		}else
		{
				return false;
		}
}

/****************************************************************************
 Function
     PostFarmerRX

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue

****************************************************************************/
bool PostFarmerRX( ES_Event ThisEvent )
{
  return ES_PostToService( MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunFarmerRX

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise
****************************************************************************/
ES_Event RunFarmerRX( ES_Event ThisEvent )
{
	ES_Event ReturnEvent;
	ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

	// if this event is received valid packet, analyze the received data
	if(ThisEvent.EventType == VALID_PACKET_RECEIVED){
			// get data packet length from the event parameter
			DataPacket_Length = ThisEvent.EventParam;
			// copy elements from the buffer in FarmerTX module to local one
			for(int i = 0; i < DataPacket_Length; i++){
					DataPacket[i] = GetRXBuffer(i);
			}
			// print out the received packet data
			PrintOutReceivedPacket();
			// call analyzing function to process the data packet
			AnalyzeRXPacket();
	}
	
	return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/*
 *	This function clear data packet ocntent
 */
static void ClearDataPacket(){
	for(int i = 0; i < LARGEST_FRAME_DATA; i++){
		// set all index of data packet to 0
		DataPacket[i] = 0;
	}
}

/*
 *	This function analyze the recieved data packet
 */
static void AnalyzeRXPacket(){
	API_id = DataPacket[0];
	// if api id is 0x89, this is transmit status message
	if(API_id == API_TX_STATUS_ID){
		#ifdef DEBUGGING_EVENT
		printf("Received Transmit Status Message\r\n");
		#endif

		frame_id = DataPacket[1];	// set frame id
		tx_status = DataPacket[2];	// set transmit status
		ES_Event ThisEvent;
		// if transmit status is 0, this is ack
		if(tx_status == ACK_STATUS){
			#ifdef DEBUGGING_EVENT
			printf("Received ACK - transmit successful\r\n");
			#endif
			ThisEvent.EventType = ACK_RECEIVED;
			ThisEvent.EventParam = frame_id;
			PostFarmerTopSM(ThisEvent);
		}
		// if transmit status is 1, this is nack
		else if(tx_status == NACK_STATUS){
			#ifdef DEBUGGING_EVENT
			printf("Received NACK - transmit failed\r\n");
			#endif
			ThisEvent.EventType = NACK_RECEIVED;
			ThisEvent.EventParam = frame_id;
			PostFarmerTopSM(ThisEvent);
		}
	}
	// if api id is 0x81, this is receive data message
	else if(API_id == API_RX_ID){
		// save source address
		Source_Address_MSB = DataPacket[1];
		Source_Address_LSB = DataPacket[2];
		// set dog address
		Dog_Address = (Source_Address_MSB << 8) + Source_Address_LSB;

		// if data packet type is pairing acknowledge message
		if(DataPacket[5] == PAIR_ACK){
			#ifdef DEBUGGING_EVENT
			printf("Farmer received pair acknowledge message\r\n");
			#endif
			// post get req2pair ack response message to farmer top SM
			ES_Event ThisEvent;
			ThisEvent.EventType = REQ2PAIR_ACK_RECEIVED;
			ThisEvent.EventParam = Dog_Address;
			PostFarmerTopSM(ThisEvent);
		}
		// if data packet type is encryption reset request
		else if(DataPacket[5] == ENCR_RESET){
			#ifdef DEBUGGING_EVENT
			printf("Farmer received encryption reset message\r\n");
			#endif
			// post reset encryption received to farmer top SM
			ES_Event ThisEvent;
			ThisEvent.EventType = RESET_ENCRYPTION_RECEIVED;
			ThisEvent.EventParam = Dog_Address;
			PostFarmerTopSM(ThisEvent);

		}
		// if data packet type is pairing acknowledge message
		else if(DataPacket[5] == STATUS){
			#ifdef DEBUGGING_EVENT
			printf("Farmer received standard status report message\r\n");
			#endif
			
			// save status to local array
			for(int i = 0; i < STATUS_LEN; i++){
				IMU_Status[i] = DataPacket[i+6];
			}
			
			// combine every two imu status to one imu value
			for(int j = 0; j < IMU_LEN; j++){
				IMU_Value[j] = (IMU_Status[2*j] << 8) + IMU_Status[(2*j) +1];
			}
			// print out the received status
			PrintOutStatusMessage();
			// post dog report received to farmer top SM
			ES_Event ThisEvent;
			ThisEvent.EventType = DOG_REPORT_RECEIVED;
			ThisEvent.EventParam = IMU_Value[5]; // gyro z
			PostFarmerTopSM(ThisEvent);
		}
		else {
			printf("Farmer received invalid message type\r\n");

		}
	}
	
}

/*
 *	This function print out the received packet content
 */
static void PrintOutReceivedPacket(){
		#ifdef DEBUG_RECEIVE_PACKET
		printf("Received: ");
		for(int i = 0; i < DataPacket_Length; i++){
			printf("%.2x ", DataPacket[i]);
		}
		printf("\r\n");
		#endif
}

static void PrintOutStatusMessage(){
		printf("Received Status: ");
		// go through the IMU array
		for(int i = 0; i < IMU_LEN; i++){
			printf("%d ", IMU_Value[i]);
		}
		printf("\r\n");
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

