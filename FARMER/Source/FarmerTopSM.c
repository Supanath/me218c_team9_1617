/****************************************************************************
Definition for debugging
 ***************************************************************************/
#define BROADCAST_ALLOWED 			// allowing tx to broadcast message
//#define DEBUGGING_STATE 			// print out farmer's current state
//#define DEBUGGING_EVENT 			// print out current event
//#define DEMO_MANUAL_MODE 			// trigger event by keyboard input
//#define DEBUGGING_ENCRYPTION_KEY 	// print out encryption key
//#define NO_BROADCAST 				// not allow tx to broadcast message
#define NORMAL_OP_MODE 			// trigger event from controller input and timer
#define DEBUGGING_TX_DATA 		// print out transfer data packet
//#define DEBUG_ENCODER
//#define DEBUGGING_SAMPLE_DATA 	// use sample data packet for transfer

/* To do for FARMER
1. Update GetDogID method to get dog id input reading
2. Update UpdateControlState method for special function
*/

/****************************************************************************
 Module
   FarmerTopSM.c

 Description
   This is the top service for FARMER which acts as a controller of the DOG

****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for the framework and this service
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "FarmerTopSM.h"
#include "UART_Init.h"
#include "FARMER_RX.h"
#include "AnalogRead.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"	// Define PART_TM4C123GH6PM in project
#include "driverlib/gpio.h"
#include "ES_ShortTimer.h"
#include "inc/hw_types.h"
#include "inc/hw_uart.h"
#include "inc/hw_nvic.h"
#include "inc/hw_timer.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "LCD_Control.h"
/*----------------------------- Module Defines ----------------------------*/
// these times assume a 1.000mS/tick timing
#define ONE_SEC 						1000
#define HALF_SEC 						(ONE_SEC/2)
#define TWO_SEC 						(ONE_SEC*2)
#define FIVE_SEC 						(ONE_SEC*5)
#define THREE_HUNDRED_MS 		300
#define LOST_COMM_TIME 			ONE_SEC
#define INTER_MESSAGE_TIME 	THREE_HUNDRED_MS
#define ENCODER_SAMPLE_TIME 200
#define ONESHOT_TIMEOUT 		5000000
// packet byte definition
#define	START_BYTE 					0x7E
#define API_TX_ID 					0x01
#define OPTION_BYTE 				0x00
#define	MSB_LEN 						0x00
#define REQ2PAIR_FRAME_ID		0x01
#define	ENCRYPTION_FRAME_ID	0x03
#define CTRL_MAX_FRAME_ID		0xFF
#define CTRL_START_FRAME_ID	0x04
#define REQ2PAIR_DATA_LEN 	2
#define CHECKSUM_OFFSET			0xFF
#define XBEE_OVERHEAD_LEN		9
#define DATA_OVERHEAD_LEN		5
#define AD_CHANNEL_NUM			2
#define LARGEST_FRAME_DATA 	100
#define BRAKE_MASK 					0x02
#define SPECIAL_MASK				0x01

#ifdef BROADCAST_ALLOWED
#define BROADCAST_MSB_RX_ADDRESS 0xFF;
#define BROADCAST_LSB_RX_ADDRESS 0xFF;
#endif
// for debugging, in case we don't want to disturb other people with broadcast
#ifdef NO_BROADCAST
#define BROADCAST_MSB_RX_ADDRESS 0x20;
#define BROADCAST_LSB_RX_ADDRESS 0x89;
#endif

// Definition from protocol
#define REG_2_PAIR  0X01
#define PAIR_ACK    0X02
#define ENCR_KEY    0x03
#define CTRL        0x04    //this message must be encrypted
#define ENCR_RESET  0X05
#define STATUS      0x00
#define LEFT_DIR 		1
#define	RIGHT_DIR	 -1

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

static void InitLED(void);
static void BlinkLED(void);
static bool TransmitPacket(void);
static void ClearDataPacket(void);
static void WriteToUART(void);
static void ConstructPacket(bool broadcast);
static uint8_t GetDogID(void);
static void Send_RequestToPair(void);
static void ResetDogAddress(void);
static void Send_Encryption(void);
static void Send_Control(void);
static void SetDestinationAddress(void);
static void EncryptMessage(int message_len);
static uint8_t GetRandNum(void);
static void SetDogAddress(uint16_t input);
static void PrintOutXbeePacket(void);
static void SendTreatPulse(bool input);
static void ResetEncryptKeyIndex(void);
static void UpdateControlState(void);
static void Init_Port(void);
static int ConvertRPM(void);
static void ClearRXDataPacket(void);
static void ClearRXDataPacket(void);
static void ClearRXBuffer(void);
static void CopyToBuffer(void);
static void SetLed(bool input);
static void PrintUnpairMessage(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
FarmerTopState_t CurrentState;	//define currentstate variable for this SM
FarmerRXState_t CurrentRXState;	//define currentstate variable for this SM
static uint8_t MyPriority;
static uint8_t DataPacket_Length;
static uint8_t XbeePacket_Length;
static uint8_t CheckSum = 0x00;
static uint8_t frame_id = 0x01;
static uint8_t byte_remaining = 0x00;
static uint8_t dog_id = 0x00; // for dog indentification tag
static uint16_t paired_dog_address = 0x0000;
static uint8_t msb_rx_address = 0x20;
static uint8_t lsb_rx_address = 0x89;
static uint8_t encrypt_index = 0;
static int tx_index = 0;
static const int ENCRYPTION_KEY_LEN = 32;
static uint8_t FanState = 0x01;
static uint8_t DataPacket[LARGEST_FRAME_DATA]; //define datapacket array
static uint8_t EncryptKey[ENCRYPTION_KEY_LEN];
static uint8_t XbeePacket[LARGEST_FRAME_DATA + XBEE_OVERHEAD_LEN];
static const int CTRL_LEN = 4;
static uint8_t Ctrl_fwd_bwd = 0x00;
static uint8_t Ctrl_l_r = 0x00;
static uint8_t Ctrl_digital = 0x00;
static uint32_t AD_data[4];

static uint32_t encoder1_count = 0;
static uint32_t ThisPeriod_Encoder1=0;
static uint32_t ThisCapture_Encoder1=10;
static uint32_t LastCapture_Encoder1=0;
static uint32_t FlagOneShotTimeout_Encoder1=0;
static float rpm_1 = 0;
static float RPM_MULTIPLIER = 10704000;
static int direction = 0;
static const int Ctrl_Offset = 127;
static float RPM_MAX = 255;

/* Received side parameters */
static uint8_t RX_DataPacket_Length;
static uint8_t RX_CheckSum = 0x00;
static uint8_t Received_RX_CheckSum = 0x00;
static uint8_t RX_Message_MSB_Len = 0;
static uint8_t RX_Message_LSB_Len = 0; 
static uint8_t RX_DataPacket[LARGEST_FRAME_DATA] = {0}; //define datapacket array
static uint8_t RX_Buffer_Array[LARGEST_FRAME_DATA] = {0}; //define datapacket array
static int rx_index = 0;

static const int MAX_CLAP = 5;
static int CLAP_COUNTER = 0;
static bool Special_fn_activate = false;
static int status_printing_count = 0;
static const int print_target_count = 4;
static const uint32_t fwd_upper_bound = 1465;
static const uint32_t fwd_lower_bound = 275;
static const uint32_t fwd_middle = 750;
static const uint32_t fwd_middle_tolerance = 50;
static float fwd_temp = 0;
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitFarmerTopSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any 
     other required initialization for this service

****************************************************************************/
bool InitFarmerTopSM ( uint8_t Priority )
{
		ES_Event ThisEvent;
		MyPriority = Priority;
	
		/********************************************
		 Initialization Code
		 *******************************************/

		// set up I/O lines for debugging
		SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
		GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_2);
	
		// start with the lines low
		GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_2, BIT2LO);  
	
		// initialize current state to waiting to pair
		CurrentState = WaitToPair;
	  CurrentRXState = WaitFor7E;
		// Initialize variable to 0
		DataPacket_Length = 0;
		XbeePacket_Length = 0;
		encrypt_index = 0;

		// Initialize UART
		Init_UART();						// Init UART for communication with Xbee
		Init_UART_ModuleFive(); // Init UART for communication with TREAT
		Init_Port();
		
		// also need to enable NVIC and enable global interrupt
		HWREG(NVIC_EN0) |= BIT6HI; // from page 104, this is for UART1 (vector 22, bit 6)
		__enable_irq();
		
		// Initialize A/D
		 ADC_MultiInit(AD_CHANNEL_NUM);
		
		// Init Input Capture
		InitInputCapture_Encoder1();
		InitOneShotInt_Encoder1();
		
		// initially set led to unpair mode
		SetLed(false);
		
		// print unpair message
		PrintUnpairMessage();
		
		// print out initializing completion
		printf("Finished initializing Farmer RX Service\r\n");
		
		// if we want to debug value from encoder, we trigger the printout by using this timer
		#ifdef DEBUG_ENCODER
		ES_Timer_InitTimer(ENCODER_TIMER, ENCODER_SAMPLE_TIME);
		#endif
		
		// post the initial transition event
		ThisEvent.EventType = ES_INIT;
		if (ES_PostToService( MyPriority, ThisEvent) == true)
		{
				return true;
		}else
		{
				return false;
		}
}

/****************************************************************************
 Function
     PostFarmerTopSM

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue

****************************************************************************/
bool PostFarmerTopSM( ES_Event ThisEvent )
{
  return ES_PostToService( MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunFarmerTopSM

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here

****************************************************************************/
ES_Event RunFarmerTopSM( ES_Event ThisEvent )
{
  ES_Event ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  FarmerTopState_t NextState = CurrentState;
  
  switch (CurrentState){
			// if current state is wait to pair
			case WaitToPair:
					// when press c, send request to pair message
					if(((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == 'c')) || (ThisEvent.EventType == PAIR_BUTTON_PRESSED)){
							// reset paired dog address
							ResetDogAddress();
							// broadcast request to pair message
							Send_RequestToPair();
							// set next state to wait for pair response
							NextState = WaitForPairResponse;
							// Start lost communicaton timer (set to 1 s)
							ES_Timer_InitTimer(LOST_COMM_TIMER, LOST_COMM_TIME);
						
							#ifdef DEBUGGING_STATE
							printf("FAMER STATE: WAIT FOR PAIR RESPONSE\r\n");
							#endif
					}
					
					// when the encoder timer timeout, printout the rpm output
					if((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == ENCODER_TIMER)){
							UpdateControlState();
							float rpm_temp = rpm_1;
							int output = (rpm_1*Ctrl_Offset)/RPM_MAX;
							if(output > Ctrl_Offset){ output = Ctrl_Offset; }
							//printf("Forward Backward Status %d\r\n", Ctrl_fwd_bwd);
							printf("RPM: %f, direction %d, Output Status: %d\r\n", rpm_temp, direction, output + Ctrl_Offset);
							//printf("Brake Status %d\r\n", Ctrl_digital);
							// Start lost communicaton timer (set to 1 s)
							ES_Timer_InitTimer(ENCODER_TIMER, ENCODER_SAMPLE_TIME);
					}
					
					// this condition is used to test the LCD (we send 1234 so LCD should show this value)
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == 'w')){
							ES_Event NewEvent;
							NewEvent.EventType = ES_NEW_KEY;
							NewEvent.EventParam = -12345;
							PostLCD_Control(NewEvent);
					}
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == 'm')){
							PrintUnpairMessage();
					}
					
					break;

			// if current state is wait for pair response
			case WaitForPairResponse:
					// if timeout from 1s lost comm timer, go back to wait to pair state
					if((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == LOST_COMM_TIMER)){
							// set next state to wait to pair
							NextState = WaitToPair;
							PrintUnpairMessage();
							#ifdef DEBUGGING_STATE
							printf("FAMER STATE: 1s Time out, go to WAIT TO PAIR\r\n");
							#endif
					}
					// if received request to pair acknowledge (Header 0x02)
					else if(ThisEvent.EventType == REQ2PAIR_ACK_RECEIVED){
							// locally save address of DOG from the acknowledgement message
							SetDogAddress(ThisEvent.EventParam);
							// reset encryption key index to 0
							ResetEncryptKeyIndex();
							// set seed for the random number generator to
							// get unique key each time we generate them
							srand(ES_Timer_GetTime());
							// call rand once first for the random number generator to work better
							rand();
							// send encryption packet
							Send_Encryption();
							// restart inter message timer (200ms)
							ES_Timer_InitTimer(INTER_MESSAGE_TIMER, INTER_MESSAGE_TIME);
					}
					
					// if received ack for encryption message (check frame id 0x02)
					else if((ThisEvent.EventType == ACK_RECEIVED) && (ThisEvent.EventParam == ENCRYPTION_FRAME_ID)){
							// start inter message timer (300ms)
							ES_Timer_InitTimer(INTER_MESSAGE_TIMER, INTER_MESSAGE_TIME);
							// start lost communication timer
							ES_Timer_InitTimer(LOST_COMM_TIMER, LOST_COMM_TIME);
							// set next state to paired
							NextState = Paired;
							// set led for pair mode
							SetLed(true);
						
							#ifdef DEBUGGING_EVENT
							printf("PAIRING SUCCESSFUL\r\n");
							printf("FARMER Paired with DOG %.4x\r\n", paired_dog_address);
							#endif
							
							#ifdef DEBUGGING_STATE
							printf("FAMER STATE: PAIRED\r\n");
							#endif
					}
					
					// if received nack for encryption message
					else if((ThisEvent.EventType == NACK_RECEIVED) && (ThisEvent.EventParam == ENCRYPTION_FRAME_ID)){
							// resend encryption packet
							Send_Encryption();
					}
					
					break;

			// if current state is paired
			case Paired:
					// for demo mode, we want to be able to trigger event with keyboard
					#ifdef DEMO_MANUAL_MODE
					if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == 't')){
							Send_Control();
					}
					else if((ThisEvent.EventType == ES_NEW_KEY) && (ThisEvent.EventParam == 'x')){
							ResetEncryptKeyIndex();
							// if timeout, go back to wait to pair state
							NextState = WaitToPair;
							// set led for unpair mode
							SetLed(false);
							// print unpair message
							PrintUnpairMessage();
							#ifdef DEBUGGING_STATE
							printf("FAMER STATE: Time out, go to WAIT TO PAIR\r\n");
							#endif
					}			
					#endif
					
					// for normal operation, allow timeout event
					#ifdef NORMAL_OP_MODE
					// if 1s lost communication timer timeout
					if((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == LOST_COMM_TIMER)){
							// if timeout, go back to wait to pair state
							NextState = WaitToPair;
							// set led for unpair mode
							SetLed(false);
						// print unpair message
							PrintUnpairMessage();
			
						#ifdef DEBUGGING_STATE
							printf("FAMER STATE: Time out, go to WAIT TO PAIR\r\n");
							#endif
					}
					// if inter message timer timeout
					else if((ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == INTER_MESSAGE_TIMER)){
							// send control message to DOG
							Send_Control();
					}
					#endif
					
					else if(ThisEvent.EventType == PAIR_BUTTON_PRESSED){
							// when press pair button during pair state, unpair with dog
							printf("Unpairing with dog\r\n");
							ResetEncryptKeyIndex();
							// if timeout, go back to wait to pair state
							NextState = WaitToPair;
							// set led for unpair mode
							SetLed(false);
							// print unpair message
							PrintUnpairMessage();
							#ifdef DEBUGGING_STATE
							printf("FAMER STATE: Time out, go to WAIT TO PAIR\r\n");
							#endif
					}
					
					
					// if receive nack for the control message
					else if((ThisEvent.EventType == ACK_RECEIVED) && (ThisEvent.EventParam == frame_id)){
							// test restart lost comm timer when getting ack
							ES_Timer_InitTimer(LOST_COMM_TIMER, LOST_COMM_TIME);
							#ifdef DEBUGGING_EVENT
							printf("Correctly received control message ack with correct frame id: %.2x\r\n", frame_id);
							#endif
					}
					
					// if receive nack for the control message
					else if((ThisEvent.EventType == NACK_RECEIVED) && (ThisEvent.EventParam == frame_id)){
							// test restart lost comm timer when getting nack
							ES_Timer_InitTimer(LOST_COMM_TIMER, LOST_COMM_TIME);
							// reverse encrypt index
							encrypt_index -= CTRL_LEN;
							// send control packet
							Send_Control();
							#ifdef DEBUGGING_EVENT
							printf("Incorrectly received control message nack with correct frame id: %.2x\r\n", frame_id);
							#endif
					}
					
					// if receive DOG's report
					else if(ThisEvent.EventType == DOG_REPORT_RECEIVED){
							// start inter message timer (300ms)
							ES_Timer_InitTimer(INTER_MESSAGE_TIMER, INTER_MESSAGE_TIME);
							// start lost communication timer
							ES_Timer_InitTimer(LOST_COMM_TIMER, LOST_COMM_TIME);
							if(status_printing_count == 0){
								// post the report status to LCD control service for printout
								ES_Event NewEvent;
								NewEvent.EventType = ES_NEW_KEY;
								NewEvent.EventParam = ThisEvent.EventParam;
								PostLCD_Control(NewEvent);
							}
							status_printing_count++;
							if(status_printing_count >  print_target_count){
									status_printing_count = 0;
							}
					}
					
					// if receive encryption key reset request
					else if(ThisEvent.EventType == RESET_ENCRYPTION_RECEIVED){
							// reset encryption key index to 0
							ResetEncryptKeyIndex();
							// start inter message timer (300ms)
							ES_Timer_InitTimer(INTER_MESSAGE_TIMER, INTER_MESSAGE_TIME);
							// start lost communication timer
							ES_Timer_InitTimer(LOST_COMM_TIMER, LOST_COMM_TIME);
					}
					
					// if receive sound exceeds threshold event during pairing
					else if (ThisEvent.EventType == SOUND_EXCEEDS_THRESHOLD){
							// increment clap counter
							CLAP_COUNTER ++;
							// check if clap counter equals max clap
							if (CLAP_COUNTER == MAX_CLAP){
									// set special function boolean to true
									Special_fn_activate = true;
									printf("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Activate special fn\r\n");
							}
							printf("Current clap counter: %d\r\n", CLAP_COUNTER);
					}
					break;

			default :
					break;
  }
  // update current state
  CurrentState = NextState;
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/*
 *	This function reset the current index of the encryption key to zero
 */
static void ResetEncryptKeyIndex(){
	// reset encryption key index to 0
	encrypt_index = 0;
}

/*
 *	This function reset current paired dog address to zero
 */
static void ResetDogAddress(){
	paired_dog_address = 0x0000;
}

/*
 *	This function set current dog address to the input
 */
static void SetDogAddress(uint16_t input){
	paired_dog_address = input;
}

/*
 *	This function read the current dog id and set the variable accordingly
 */
static uint8_t GetDogID(){
	printf("In dog select function\r\n");
	//printf("Read pin 5: %d\r\n", GPIOPinRead(GPIO_PORTB_BASE, GPIO_PIN_5));
	if(GPIOPinRead(GPIO_PORTB_BASE, GPIO_PIN_5) == 0){
		printf("Choose dog 1\r\n");
		dog_id = 0x01;
	}
	else if(GPIOPinRead(GPIO_PORTB_BASE, GPIO_PIN_6) == 0){
		printf("Choose dog 2\r\n");
		dog_id = 0x02;
	}
	else if(GPIOPinRead(GPIO_PORTB_BASE, GPIO_PIN_7) == 0){
		printf("Choose dog 3\r\n");
		dog_id = 0x03;
	}
	else{
		printf("Dog not selected\r\n");
		dog_id = 0x08;
	}
	return dog_id; // currently, we hard code this
}

/*
 *	This function construct a packet to be transmitted with option of
 *	broadcasting to all listenning devices
 */
static void ConstructPacket(bool broadcast){
		XbeePacket[0] = START_BYTE;
		XbeePacket[1] = MSB_LEN;
		XbeePacket[2] = DataPacket_Length + DATA_OVERHEAD_LEN;
		XbeePacket[3] = API_TX_ID;
		CheckSum = API_TX_ID; // start increment checksum
		XbeePacket[4] = frame_id;
		CheckSum += frame_id;
		// for broadcasting, the destination address will be 0xFFFF
		if(broadcast){
			XbeePacket[5] = BROADCAST_MSB_RX_ADDRESS;
			CheckSum += BROADCAST_MSB_RX_ADDRESS;
			XbeePacket[6] = BROADCAST_LSB_RX_ADDRESS;
			CheckSum += BROADCAST_LSB_RX_ADDRESS;
		}
		// for non broadcasting transmit
		else{
			XbeePacket[5] = msb_rx_address;
			CheckSum += msb_rx_address;
			XbeePacket[6] = lsb_rx_address;
			CheckSum += lsb_rx_address;
		}	
		XbeePacket[7] = OPTION_BYTE;
		CheckSum += OPTION_BYTE;
		// copy data packet to the xbee packet
		for(int i = 0; i < DataPacket_Length; i++){
			XbeePacket[i+8] = DataPacket[i];
			CheckSum += DataPacket[i];
		}
		// calculate checksum value
		XbeePacket[DataPacket_Length + 8] = CHECKSUM_OFFSET - CheckSum;
		// total packet length 
		XbeePacket_Length = DataPacket_Length + XBEE_OVERHEAD_LEN;
}

/*
 *	This function clear the content of data packet array
 */
static void ClearDataPacket(){
		for(int i = 0; i < LARGEST_FRAME_DATA; i++){
			// set all index of data packet to 0
			DataPacket[i] = i;
		}
}

/*
 *	This function print out the xbee packet
 */
static void PrintOutXbeePacket(){
		#ifdef DEBUGGING_TX_DATA
		for(int i = 0; i < XbeePacket_Length; i++){
			printf("%.2x ", XbeePacket[i]);
		}
		printf("\r\n");
		#endif
}

/*
 *	This function form request to pair meesage and send out
 */
static void Send_RequestToPair(){
		#ifdef DEBUGGING_EVENT
		printf("Send Req to pair Message through Zigbee\r\n");
		#endif
		// first clear the data packet
		ClearDataPacket();
		// set request to pair packet length to 2
		DataPacket_Length = REQ2PAIR_DATA_LEN;
		// first byte is the header
		DataPacket[0] = REG_2_PAIR;
		// second byte is the dog id
		DataPacket[1] = GetDogID();
		// for request to pair, use frame_id = 0x01
		frame_id = 0x01;
	
		/* For debugging transmit message*/
		#ifdef DEBUGGING_SAMPLE_DATA
		DataPacket_Length = 10;
		DataPacket[0] = 0x01;
		DataPacket[1] = 0x02;
		DataPacket[2] = 0x03;
		DataPacket[3] = 0x04;
		DataPacket[4] = 0x05;
		DataPacket[5] = 0x06;
		DataPacket[6] = 0x07;
		DataPacket[7] = 0x08;
		DataPacket[8] = 0x09;
		DataPacket[9] = 0x10;
		#endif

		// construct packet
		ConstructPacket(true);
		// print out xbee packet
		PrintOutXbeePacket();
		// transmit it through UART to the Xbee
		TransmitPacket();
}

static void PrintUnpairMessage(){
		ES_Event NewEvent;
		NewEvent.EventType = PRINT_UNPAIR;
		PostLCD_Control(NewEvent);
}

/*
 *	This function return random 8 bit integer
 */
static uint8_t GetRandNum(){
		// return random integer between 0 and 255
		return rand() % 256;
}

/*
 *	This function form encryption key message and transmit
 */
static void Send_Encryption(){
		#ifdef DEBUGGING_EVENT
		printf("Send Encryption Key Message through Zigbee\r\n");
		#endif

		// reset encryption index
		encrypt_index = 0;
		// first clear the data packet
		ClearDataPacket();
		// request to pair packet has length of 2 bytes
		DataPacket_Length = ENCRYPTION_KEY_LEN + 1;
		// first byte is the header
		DataPacket[0] = ENCR_KEY;
		
		// fill data packet with random number
		for(int i = 0; i < ENCRYPTION_KEY_LEN; i++){
			// add random value to encryption key array
			EncryptKey[i] = GetRandNum();
			// add the encryption key to data packet
			DataPacket[i+1] = EncryptKey[i];
		}

		// for debugging, print out encryption key
		#ifdef DEBUGGING_ENCRYPTION_KEY
		printf("Encryption Key: \r\n");
		for(int i = 0; i < ENCRYPTION_KEY_LEN; i++){
			printf("%.2x ", EncryptKey[i]);
		}
		printf("\r\n");
		#endif

		// set frame id specifically for encryption key message
		frame_id = ENCRYPTION_FRAME_ID;
		// set destination address
		SetDestinationAddress();
		// construct packet
		ConstructPacket(false);
		// print out xbee packet
		PrintOutXbeePacket();
		// transmit it through UART to the Xbee
		TransmitPacket();
}

/*
 *	This function form control message and transmit
 */
static void Send_Control(){
		#ifdef DEBUGGING_EVENT
		printf("Send Control Message through Zigbee\r\n");
		#endif

		// first clear the data packet
		ClearDataPacket();
		// request to pair packet has length of 2 bytes
		DataPacket_Length = CTRL_LEN;
		// first byte is the header
		DataPacket[0] = CTRL;
		// read current control state
		UpdateControlState();
		// fill data packet with control value
		DataPacket[1] = Ctrl_fwd_bwd; //forward or backward
		DataPacket[2] = Ctrl_l_r; //left or right
		DataPacket[3] = Ctrl_digital; //digital
		
		// set frame id (increment by one everytime we send message)
		frame_id++;
		// reset frame_id to 0x04 if it reaches 0xff
		if(frame_id == CTRL_MAX_FRAME_ID){ frame_id = CTRL_START_FRAME_ID;}
		// set destination address
		SetDestinationAddress();
		// encrypt message
		EncryptMessage(CTRL_LEN);
		// construct packet
		ConstructPacket(false);
		// print out xbee packet
		PrintOutXbeePacket();
		// transmit it through UART to the Xbee
		TransmitPacket();
}

/*
 *	This function read the current control state and set the variables
 */
static void UpdateControlState(){
		// read analog input for forward/backward control input
		ADC_MultiRead(AD_data);
		uint32_t forward = AD_data[0]; // index 0 is input from PE0
		uint32_t sound = AD_data[1]; // index 1 is input from PE1
		printf("Potentiometer value: %d\r\n", AD_data[0]);
	
		if((forward >= (fwd_middle - fwd_middle_tolerance)) && (forward <= (fwd_middle + fwd_middle_tolerance))){
				Ctrl_fwd_bwd = Ctrl_Offset;
		}
		else if(forward > fwd_upper_bound){
				Ctrl_fwd_bwd = 0;
		}
		else if(forward < fwd_lower_bound){
				Ctrl_fwd_bwd = 255;
		}
		else if(forward > fwd_middle + fwd_middle_tolerance){
				fwd_temp = ((fwd_upper_bound - forward) * 127/(fwd_upper_bound - fwd_middle));
				Ctrl_fwd_bwd = (uint32_t) fwd_temp;
		}
		else{
				fwd_temp = (((fwd_middle - forward) * 127/(fwd_middle - fwd_lower_bound))) +127;
				Ctrl_fwd_bwd = (uint32_t) fwd_temp;
		}
		printf("Control forward: %d\r\n", Ctrl_fwd_bwd);
		// convert the received rpm to int from 0 to 127
		int converted_rpm = ConvertRPM();
		// not turning
		if(direction == 0){
			Ctrl_l_r = Ctrl_Offset;
		}
		// clamp the calculated value
		if(converted_rpm > Ctrl_Offset) { converted_rpm = Ctrl_Offset; }
		// turning left
		else if(direction == LEFT_DIR){
			Ctrl_l_r = Ctrl_Offset + converted_rpm;
		}
		// turning right
		else if(direction == RIGHT_DIR){
			Ctrl_l_r = Ctrl_Offset - converted_rpm;
		}
		// clamp the control value to be with in 0 to 255
		if(Ctrl_l_r > 255){ Ctrl_l_r = 255; }
		if(Ctrl_l_r < 0){ Ctrl_l_r = 0; }
		//Ctrl_l_r = 0x22;
		Ctrl_digital = 0;
		if(GPIOPinRead(GPIO_PORTC_BASE, GPIO_PIN_6) == BIT6HI){
			Ctrl_digital |= BRAKE_MASK;
		}
		
		if(Special_fn_activate){
			// set the special function bit
			Ctrl_digital |= SPECIAL_MASK;
			// set activation bool to false
			Special_fn_activate = false;
			// reset clap counter
			CLAP_COUNTER = 0;
		}
}


static void SetLed(bool input){
		if(input){
				GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_2,BIT5HI);
				GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_3,BIT5LO);
		}
		else{
				GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_2,BIT5LO);
				GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_3,BIT5HI);
		}
}

/*
 *	This function set the msb and lsb variable accordingly to the target address
 */
static void SetDestinationAddress(){
		msb_rx_address = paired_dog_address >> 8;
		lsb_rx_address = paired_dog_address & 0x00ff;
}

/*
 *	This function transmit packet by start writing to UART pin out
 *  We need to call ConstructPacket before calling this function
 */
static bool TransmitPacket(){
		// set remaining number of byte to transmit to the xbee packet length
		byte_remaining = XbeePacket_Length;
		// reset index to 0
		tx_index = 0;
		
		// check if UART Transmit FIFO empty bit is set, this mean we can send
		if((HWREG(UART1_BASE + UART_O_FR) & UART_FR_TXFE) == UART_FR_TXFE){
			// write data to UART
			WriteToUART();
			
			// this if loop is for increasing effciency of trasmitting
			if ((HWREG(UART1_BASE + UART_O_FR) & UART_FR_TXFE) == UART_FR_TXFE){
				// write data to UART
				WriteToUART();
			}
			//enable TX interrupt
			HWREG(UART1_BASE + UART_O_IM) |= UART_IM_TXIM;
			//return success
			return true;
		}
		else{
			// else return failure
			return false;
		}
}

/*
 *	This function is the ISR for the UART1 module
 */
void FARMER_UART1_ISR(){
		// check if UART1 interrupt flag for receiver is set
		if (HWREG(UART1_BASE+UART_O_MIS)& UART_MIS_RXMIS){
			// clear RX interrupt flag
			HWREG(UART1_BASE + UART_O_ICR) = UART_ICR_RXIC;
			uint8_t Received_byte = HWREG(UART1_BASE + UART_O_DR);
			
			// RX State Machine in ISR
			// We put this state machine here so we don't miss any incoming byte
			FarmerRXState_t NextRXState = CurrentRXState;
			switch (CurrentRXState){
				case WaitFor7E:
						// if receive incoming start byte
						if(Received_byte == START_BYTE){
								// first clear current data packet
								ClearRXDataPacket();
								// reset Checksums, received index to 0
								RX_CheckSum = 0;
								Received_RX_CheckSum = 0;
								rx_index = 0;
								// set next state to wait for msb
								NextRXState = WaitForMSBLen;
						}
						break;
						
				// if current state is wait for MSB
				case WaitForMSBLen:
							// save msb length of the received packet
							RX_Message_MSB_Len = Received_byte;
							// set next state to wait for lsb
							NextRXState = WaitForLSBLen;
						break;
					
				// if current state is wait for LSB
				case WaitForLSBLen:
							// save lsb length of the received packet
							RX_Message_LSB_Len = Received_byte;
							// form data packet length from the received msb and lsb
							RX_DataPacket_Length = (RX_Message_MSB_Len << 8) + RX_Message_LSB_Len;
							// set next state to get data packet
							NextRXState = GetDataPacket;
						break;
						
				// if current state is wait for get data packet
				case GetDataPacket:
							// put received data into DataPacket array
							RX_DataPacket[rx_index] = Received_byte;
							// add the data up to check sum
							RX_CheckSum += Received_byte;
							// increment the received index
							rx_index++;
							// compare index to the received index, if equal go to check sum state
							if(rx_index == RX_DataPacket_Length){
								// compute final check sum by subtracting from 0xFF
								RX_CheckSum = CHECKSUM_OFFSET - RX_CheckSum;
								// set next state to get check sum
								NextRXState = GetCheckSum;
							}
						break;

				// if current state is get check sum
				case GetCheckSum:
						// set check sum to the byte received
						Received_RX_CheckSum = Received_byte;
						// check if it equal to our calculated check sum
						if(Received_RX_CheckSum == RX_CheckSum){
							CopyToBuffer();
							// post received byte to FARMER_RX service
							ES_Event ThisEvent;
							ThisEvent.EventType = VALID_PACKET_RECEIVED;
							ThisEvent.EventParam = RX_DataPacket_Length;
							PostFarmerRX(ThisEvent);
						}
						// set next state to wait for start delimiter bytes
						NextRXState = WaitFor7E;
						break;
				
				default:
						break;
			}
			// update current rx state
			CurrentRXState = NextRXState;
		}
	
		// check if UART1 interrupt flag for transmitter is set
		else if(HWREG(UART1_BASE+UART_O_MIS)& UART_MIS_TXMIS){
			
			// write data to UART but first wait till FIFO empty
			if((HWREG(UART1_BASE + UART_O_FR) & UART_FR_TXFE) == UART_FR_TXFE){
					// clear TX interrupt flag	
					HWREG(UART1_BASE + UART_O_ICR) = UART_ICR_TXIC;
					// write data to UART
					WriteToUART();
			}
			// check number of byte remaining
			if(byte_remaining == 0){
				// if byte remaining is zero, disable interrupt on TX
				HWREG(UART1_BASE + UART_O_IM) &= ~UART_IM_TXIM;
			}
		}
}

/*
 *	This function copy data from xbeepacket array to the UART pin out
 */
static void WriteToUART(){
		// write XbeePacket[tx_index] to UARTDR
		HWREG(UART1_BASE + UART_O_DR) = (HWREG(UART1_BASE + UART_O_DR) & ~UART_DR_DATA_M) + XbeePacket[tx_index];
		// decrement byte remaining
		byte_remaining--;
		// increment index
		tx_index++;
}

/*
 *	This function encrypts data packet
 */
static void EncryptMessage(int message_len){
		// go through the data packet and xor by encrypt key index by index
		for(int i = 0; i < message_len; i++){
			DataPacket[i] = DataPacket[i] ^ EncryptKey[encrypt_index];
			// increment encryption key index
			encrypt_index++;
			// reset index to zero if it reaches 32
			if(encrypt_index == ENCRYPTION_KEY_LEN){
				encrypt_index = 0;
			}
		}
		#ifdef DEBUGGING_ENCRYPTION_KEY
		printf("Header encryption key: %.2x\r\n", EncryptKey[encrypt_index]);
		#endif
}

/*
 *	This function initializes the components for encoder input capture (PC4)
 */
void InitInputCapture_Encoder1( void ){
		// start by enabling the clock to the timer (Wide Timer 0)
		HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0;
		// enable the clock to Port C
		HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
		// since we added this Port C clock init, we can immediately start
		// into configuring the timer, no need for further delay
		// make sure that timer (Timer A) is disabled before configuring
		HWREG(WTIMER0_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEN;
		// set it up in 32bit wide (individual, not concatenated) mode
		// the constant name derives from the 16/32 bit timer, but this is a 32/64
		// bit timer so we are setting the 32bit mode
		HWREG(WTIMER0_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
		// we want to use the full 32 bit count, so initialize the Interval Load
		// register to 0xffff.ffff (its default value :-)
		HWREG(WTIMER0_BASE+TIMER_O_TAILR) = 0xffffffff;
		// set up timer A in capture mode (TAMR=3, TAAMS = 0),
		// for edge time (TACMR = 1) and up-counting (TACDIR = 1)
		HWREG(WTIMER0_BASE+TIMER_O_TAMR) =
		(HWREG(WTIMER0_BASE+TIMER_O_TAMR) & ~TIMER_TAMR_TAAMS) |
		(TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
		// To set the event to rising edge, we need to modify the TAEVENT bits
		// in GPTMCTL. Rising edge = 00, so we clear the TAEVENT bits
		HWREG(WTIMER0_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;
		// Now Set up the port to do the capture (clock was enabled earlier)
		// start by setting the alternate function for Port C bit 4 (WT0CCP0)
		HWREG(GPIO_PORTC_BASE+GPIO_O_AFSEL) |= BIT4HI;
		// Then, map bit 4's alternate function to WT0CCP0
		// 7 is the mux value to select WT0CCP0, 16 to shift it over to the
		// right nibble for bit 4 (4 bits/nibble * 4 bits)
		HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) =
		(HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) & 0xfff0ffff) + (7<<16);
		// Enable pin on Port C for digital I/O
		HWREG(GPIO_PORTC_BASE+GPIO_O_DEN) |= BIT4HI;
		// make pin 4 on Port C into an input
		HWREG(GPIO_PORTC_BASE+GPIO_O_DIR) &= BIT4LO;
		// back to the timer to enable a local capture interrupt
		HWREG(WTIMER0_BASE+TIMER_O_IMR) |= TIMER_IMR_CAEIM;
		// enable the Timer A in Wide Timer 0 interrupt in the NVIC
		// it is interrupt number 94 so appears in EN2 at bit 30
		HWREG(NVIC_EN2) |= BIT30HI;
		// make sure interrupts are enabled globally
		__enable_irq();
		// now kick the timer off by enabling it and enabling the timer to
		// stall while stopped by the debugger
		HWREG(WTIMER0_BASE+TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
}

/*
 *	This function is the ISR for the encoder input capture
 */
void InputCaptureResponse_Encoder1( void ){
		// start by clearing the source of the interrupt, the input capture event
		HWREG(WTIMER0_BASE+TIMER_O_ICR) = TIMER_ICR_CAECINT;
		//start the one shot timer
		StartOneShot_Encoder1();
		//clear the Flag for One shot because now we are at a new edge
		FlagOneShotTimeout_Encoder1=0;
		// now grab the captured value and calculate the period
		ThisCapture_Encoder1 = HWREG(WTIMER0_BASE+TIMER_O_TAR);
		ThisPeriod_Encoder1 = ThisCapture_Encoder1 - LastCapture_Encoder1;
		// update LastCapture to prepare for the next edge
		LastCapture_Encoder1 = ThisCapture_Encoder1;
		encoder1_count ++; //increase the encoder_count from the detected pulse
		rpm_1 = RPM_MULTIPLIER/ThisPeriod_Encoder1;
		//printf("RPM is %f\r\n", rpm_1);
		// detect the state of PC5 to identify current direction of the wheel
		if(GPIOPinRead(GPIO_PORTC_BASE, GPIO_PIN_5) == BIT5HI){
			direction = LEFT_DIR;
		}
		else{
			direction = RIGHT_DIR;
		}
}

/*
 *	This function returns the value of the wheel's current RPM
 */
double get_RPM_Encoder1(void){
		return rpm_1;
}

/*
 *	This function initializes one shot timer for encoder (detecting stopping event)
 */
void InitOneShotInt_Encoder1( void ){
		// start by enabling the clock to the timer (Wide Timer 0)
		HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0;
		// kill a few cycles to let the clock get going
		while((HWREG(SYSCTL_PRWTIMER) & SYSCTL_PRWTIMER_R0) != SYSCTL_PRWTIMER_R0)
		{
		}
		// make sure that timer (Timer B) is disabled before configuring
		HWREG(WTIMER0_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TBEN; //TBEN = Bit8
		// set it up in 32bit wide (individual, not concatenated) mode
		// the constant name derives from the 16/32 bit timer, but this is a 32/64
		// bit timer so we are setting the 32bit mode
		HWREG(WTIMER0_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT; //bits 0-2 = 0x04
		// set up timer B in 1-shot mode so that it disables timer on timeouts
		// first mask off the TAMR field (bits 0:1) then set the value for
		// 1-shot mode = 0x01
		HWREG(WTIMER0_BASE+TIMER_O_TBMR) =
		(HWREG(WTIMER0_BASE+TIMER_O_TBMR)& ~TIMER_TBMR_TBMR_M)|
		TIMER_TBMR_TBMR_1_SHOT;
		// set timeout
		HWREG(WTIMER0_BASE+TIMER_O_TBILR) = ONESHOT_TIMEOUT;
		// enable a local timeout interrupt. TBTOIM = bit 8
		HWREG(WTIMER0_BASE+TIMER_O_IMR) |= TIMER_IMR_TBTOIM; // 8
		// enable the Timer B in Wide Timer 0 interrupt in the NVIC
		// it is interrupt number 95 so appears in EN2 at bit 30
		HWREG(NVIC_EN2) |= BIT31HI; 
		// make sure interrupts are enabled globally
		__enable_irq();
		// now kick the timer off by enabling it and enabling the timer to
		// stall while stopped by the debugger. TAEN = Bit0, TASTALL = bit1
		HWREG(WTIMER0_BASE+TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
}

/*
 *	This function starts the one shot timer
 */
void StartOneShot_Encoder1( void ){
		// kick the timer off by enabling it and enabling the timer to
		// stall while stopped by the debugger
		HWREG(WTIMER0_BASE+TIMER_O_CTL) |= (TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
}

/*
 *	This function is the ISR for one shot timeout
 */
void OneShotIntResponse_Encoder1( void ){
		// start by clearing the source of the interrupt
		HWREG(WTIMER0_BASE+TIMER_O_ICR) = TIMER_ICR_TBTOCINT;
		rpm_1 = 0;
		direction = 0;
		//printf("In one shot ISR after clearing\n\r");
		FlagOneShotTimeout_Encoder1=1;
}

/*
 *	This function initializes PC5 and PC6 
 */
static void Init_Port(){
		//Initialize clock to Port A
		SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	
		//Initialize clock to Port B
		SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
	
		//Initialize clock to Port C
		SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
	
		//Initialize PA2,3,4,5,6 for LED output
		GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_2);
		GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_3);
		GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_4);
		GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_5);
		GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_6);
		
		//Initialize PA7 for Sound Control
		GPIOPinTypeGPIOOutput(GPIO_PORTA_BASE, GPIO_PIN_7);
	
		//Initialize PB5,6,7 for Dog Select
		GPIOPinTypeGPIOInput(GPIO_PORTB_BASE, GPIO_PIN_5);
		GPIOPinTypeGPIOInput(GPIO_PORTB_BASE, GPIO_PIN_6);
		GPIOPinTypeGPIOInput(GPIO_PORTB_BASE, GPIO_PIN_7);
	
		//Initialize PC5 for Encoder directin reading
		GPIOPinTypeGPIOInput(GPIO_PORTC_BASE, GPIO_PIN_5);
		
		//Initialize PC6 and PC7 for brake input and pair button input
		GPIOPinTypeGPIOInput(GPIO_PORTC_BASE, GPIO_PIN_6);
		GPIOPinTypeGPIOInput(GPIO_PORTC_BASE, GPIO_PIN_7);
		
		
		GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_4, BIT4HI);
		GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_5, BIT5HI);
		GPIOPinWrite(GPIO_PORTA_BASE, GPIO_PIN_6, BIT6HI);
}

/*
 *	This function converts the current RPM to output control value
 */
static int ConvertRPM(){
		return (rpm_1*Ctrl_Offset)/RPM_MAX;
}

/*
 *	This function clears data packet ocntent
 */
static void ClearRXDataPacket(){
	for(int i = 0; i < LARGEST_FRAME_DATA; i++){
		// set all index of data packet to 0
		RX_DataPacket[i] = 0;
	}
}

/*
 *	This function clear the RX buffer array
 */
static void ClearRXBuffer(){
	for(int i = 0; i < LARGEST_FRAME_DATA; i++){
		// set all index of data packet to 0
		RX_Buffer_Array[i] = 0;
	}
}

/*
 *	This function copies the elements from received data packet
 *  array to the buffer array
 */
static void CopyToBuffer(){
	// ClearRXBuffer();
	for(int i = 0; i < LARGEST_FRAME_DATA; i++){
		// set all index of data packet to 0
		RX_Buffer_Array[i] = RX_DataPacket[i];
	}
}

/*
 *	This function return the element at specific index of the buffer array
 */
uint8_t GetRXBuffer(int index){
	return RX_Buffer_Array[index];
}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

