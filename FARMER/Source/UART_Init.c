/****************************************************************************
 Module
   UART_Init.c

 Description
   This is the module for initializing UART (PB0, PB1, PE5)


****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for the framework and this service
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "UART_Init.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"	// Define PART_TM4C123GH6PM in project
#include "driverlib/gpio.h"
#include "ES_ShortTimer.h"
#include "inc/hw_types.h"
#include "inc/hw_uart.h"
#include "inc/hw_nvic.h"
#include "inc/hw_timer.h"

/*---------------------------- Module Functions ---------------------------*/

void Init_UART(){
		// Enable the UART module using the RCGCUART region
		// We enable UART1 module
		HWREG(SYSCTL_RCGCUART) |= SYSCTL_RCGCUART_R1;

		// Enable Clock to port B (We are using PB0 and PB1)
		HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;

		// wait for UART1 and GPIO Port B to be ready
		while((HWREG(SYSCTL_PRUART) & SYSCTL_PRUART_R1)!= SYSCTL_PRUART_R1){}
		while((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R1)!= SYSCTL_PRGPIO_R1){}

		// configure GPIO pin for in/out and drive type
		HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT0HI | BIT1HI); //digital enable
		HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= BIT1HI; //direction PB1 output
		HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) &= ~BIT0HI; //PB0 input

		// select alternate function for the UART pins
		HWREG(GPIO_PORTB_BASE + GPIO_O_AFSEL) |= (BIT0HI | BIT1HI);

		// configure the PMCn fields in the GPIOPCTL register to assign the UART pins
		// see tiva's datasheet page 1351
		// write 1 to PB0 to select U1Rx and 1 to PB1 to select U1Tx
		HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) = (HWREG(GPIO_PORTB_BASE + GPIO_O_PCTL) & 0xffffff00) | 0x00000011;
			
		// disable UART module 1
		HWREG(UART1_BASE + UART_O_CTL) &= ~UART_CTL_UARTEN;
			
		// to get 9600 Baud, we need BRDI = 260 and BRDF = 27
		// this is from 40MHz/16*9600 = 260.4166 so BRDF = int(.4166*64 + .5) = 27
		HWREG(UART1_BASE + UART_O_IBRD) = 0x104;
		HWREG(UART1_BASE + UART_O_FBRD) = 0x1B;
		
		// write the desired serial parameters to the UARTLCRH register
		// we can just set this because this register come out of reset with all 0
		HWREG(UART1_BASE + UART_O_LCRH) = UART_LCRH_WLEN_8;
		
		// set RXE, TXE, EOT and UARTEN (enable UART) in the control register
		HWREG(UART1_BASE + UART_O_CTL) |= (UART_CTL_RXE| UART_CTL_TXE | UART_CTL_EOT | UART_CTL_UARTEN);
}

void Init_UART_ModuleFive(){
		// Enable the UART module using the RCGCUART region
		// We enable UART5 module
		HWREG(SYSCTL_RCGCUART) |= SYSCTL_RCGCUART_R5; 

		// Enable Clock to port E (We are using PE4 and PE5)
		HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R4;

		// wait for UART5 and GPIO Port E to be ready
		while((HWREG(SYSCTL_PRUART) & SYSCTL_PRUART_R5)!= SYSCTL_PRUART_R5){}
		while((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R4)!= SYSCTL_PRGPIO_R4){}

		// configure GPIO pin for in/out and drive type
		HWREG(GPIO_PORTE_BASE + GPIO_O_DEN) |= (BIT4HI | BIT5HI); //digital enable
		HWREG(GPIO_PORTE_BASE + GPIO_O_DIR) |= BIT5HI; //direction PE5 output
		HWREG(GPIO_PORTE_BASE + GPIO_O_DIR) &= (~BIT4HI); //PB0 input

		// select alternate function for the UART pins
		HWREG(GPIO_PORTE_BASE + GPIO_O_AFSEL) |= (BIT4HI | BIT5HI);

		// configure the PMCn fields in the GPIOPCTL register to assign the UART pins
		// see tiva's datasheet page 1351
		// write 1 to PB0 to select U1Rx and 1 to PB1 to select U1Tx
		HWREG(GPIO_PORTE_BASE + GPIO_O_PCTL) = (HWREG(GPIO_PORTE_BASE + GPIO_O_PCTL) & 0xff00ffff) | 0x00110000;
			
		// disable UART module 5
		HWREG(UART5_BASE + UART_O_CTL) &= ~UART_CTL_UARTEN;
			
		// to get 150.2404 Baud, we need BRDI = 16640 and BRDF =1
		// this is from 40MHz/16*150.2404 = 16640 so BRDF = int(0*64 + .5) = 1
		HWREG(UART5_BASE + UART_O_IBRD) = 0x4100;
		HWREG(UART5_BASE + UART_O_FBRD) = 0x01;
		
		// write the desired serial parameters to the UARTLCRH register
		// we can just set this because this register come out of reset with all 0
		HWREG(UART5_BASE + UART_O_LCRH) = UART_LCRH_WLEN_8; //8 bit length
		
		// set RXE, TXE, EOT and UARTEN (enable UART) in the control register
		HWREG(UART5_BASE + UART_O_CTL) |= (UART_CTL_RXE| UART_CTL_TXE | UART_CTL_EOT | UART_CTL_UARTEN);
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

