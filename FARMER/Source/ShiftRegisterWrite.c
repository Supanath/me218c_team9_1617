//#define TEST
/****************************************************************************
 Module
   ShiftRegisterWrite.c

 Revision
   1.0.1

 Description
   This module acts as the low level interface to a write only shift register.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/11/15 19:55 jec     first pass
 
****************************************************************************/
// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "BITDEFS.H"

 #include "ShiftRegisterWrite.h"//suggested by Luke to link up the header files

// readability defines
#define DATA GPIO_PIN_2

#define SCLK GPIO_PIN_3
#define SCLK_HI BIT3HI
#define SCLK_LO BIT3LO

#define RCLK GPIO_PIN_4
#define RCLK_LO BIT4LO
#define RCLK_HI BIT4HI

#define GET_MSB_IN_LSB(x) ((x & 0x80)>>7)

 #define ALL_BITS (0xff<<2) 

// an image of the last 8 bits written to the shift register
static uint8_t LocalRegisterImage=0;

// Create your own function header comment
void SR_Init(void){

  // set up port B by enabling the peripheral clock and setting the direction
  // of PB0, PB1 & PB2 to output

	//turn on bit 1, enable port B
	HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1;//enabling peripheral clock
	//wait a bit till it is fully open
	while ((HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R1) != SYSCTL_PRGPIO_R1);

	//set bits to be used as digital I/O lines
	HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= GPIO_PIN_2;//bit 2
	HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= GPIO_PIN_3;//bit 3
	HWREG(GPIO_PORTB_BASE+GPIO_O_DEN) |= GPIO_PIN_4;//bit 4

	//set it to output
	HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) |= GPIO_PIN_2;//bit 2
	HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) |= GPIO_PIN_3;//bit 3
	HWREG(GPIO_PORTB_BASE+GPIO_O_DIR) |= GPIO_PIN_4;//bit 4

 
    // start with the data & sclk lines low and the RCLK line high
 	//that means to write data-bit0 low, shift clock- bit1 low, and register clock - bit2 high
 	HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA +  ALL_BITS)) &= (~GPIO_PIN_2); // write data-bit 0 low
 	HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA +  ALL_BITS)) &= (~GPIO_PIN_3); // write shift clock-bit 1 low
 	HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA +  ALL_BITS)) |= (GPIO_PIN_4); // write register clock-bit 2 high

}

// Create your own function header comment
uint8_t SR_GetCurrentRegister(void){
  return LocalRegisterImage;
}

// Create your own function header comment
void SR_Write(uint8_t NewValue){

  uint8_t BitCounter;
  uint8_t tempNewValue;
  LocalRegisterImage = NewValue; // save a local copy, local to the module I think
  tempNewValue=NewValue;

  // lower the register clock
  //register clock is port B bit 2, write in low
  HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA +  ALL_BITS)) &= ~GPIO_PIN_4; // write register clock-bit 2 low

  // edge the MSB of NewValue, put it into the LSB position and output
  for (BitCounter=0; BitCounter<8;BitCounter++){
  	//take the MSB and write to data
  	if ((tempNewValue & BIT7HI)>0 ) {// the the most significant bit is 1
  		HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA +  ALL_BITS)) |= (GPIO_PIN_2); //write 1 to the data-bit 2
  	}
  	else{
  		HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA +  ALL_BITS)) &= (~GPIO_PIN_2); //write 0 to the data-bit 2
  	}
  // shift out the data while pulsing the serial clock 
  //write bit 1 high then low, this thing triggers on falling Isolate
  HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA +  ALL_BITS)) |= (GPIO_PIN_3); // write shift clock-bit 1 high
  HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA +  ALL_BITS)) &= (~GPIO_PIN_3); // write shift clock-bit 1 low, finish pulsing

  tempNewValue=(tempNewValue<<1); // shift the temp new value so we update the MSB
  }

// raise the register clock to latch the new data
  HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA +  ALL_BITS)) |= GPIO_PIN_4; // write register clock-bit 2 low
  
}

#ifdef TEST
int main(){
	SR_Init();//initialize
	uint8_t testNum = 255; //check by looking at the output of the circuit with LED or oscciloscope
	//uint8_t testNum = (GPIO_PIN_2 | GPIO_PIN_4  | GPIO_PIN_6);
	SR_Write(testNum);

}
#endif
