//#define TEST

/***************************************************************************
 Module
   LCDWrite.c

 Revision
   1.0.1

 Description
   This module acts as the low level interface to an LCD display in 4-bit
   mode with the actual data written to a shift register.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/12/15 15:15 jec     first pass
 
****************************************************************************/
//----------------------------- Include Files -----------------------------*/
// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>

#include "BITDEFS.H"
#include "ShiftRegisterWrite.h"
#include "LCD_Write.h"



// Private functions
static void LCD_SetData4(uint8_t NewData);
static void LCD_PulseEnable(void);
static void LCD_RegisterSelect(uint8_t WhichReg);
static void LCD_Write4(uint8_t NewData);
static void LCD_Write8(uint8_t NewData);

// module level defines
#define LSB_MASK 0x0f
#define LCD_COMMAND 0
#define LCD_DATA    1
#define NUM_INIT_STEPS 10
#define USE_4_BIT_WRITE 0x8000

// these are the iniitalization values to be written to set up the LCD
static const uint16_t InitValues[NUM_INIT_STEPS] = {
  (0x03 | USE_4_BIT_WRITE), /* multi-step process to get it into 4-bit mode */
  (0x03 | USE_4_BIT_WRITE),  //basically trying to distinguish whether this is using WriteCommand4
  (0x03 | USE_4_BIT_WRITE),  
  (0x02 | USE_4_BIT_WRITE),  //from LCD Initialization flowchart pdf
    0x20, /* 4-bit data width, 1 line, 5x7 font */
    0x08, /* turn off the display */
    0x01, /* clear the display */
    0x07, /* increment and shift with each new character */
    0x0f, /* turn on display, cursor & blink */
    0x97 /* position cursor */}; //24 character display, address is to tell it which character on display to rest on

// these are the delays between the initialization steps.
// the first delay is the power up delay so there is 1 more entry
// in this table than in the InitValues Table    
static const uint16_t InitDelays[NUM_INIT_STEPS+1] = {//consider using max for everything
    65535, /* use max delay for powerup */
     4100,
      100,
      100,
      100,
       53,
       53,
     3000,
       53,
       53,
       53
};

// place to keep track of which regiter we are writing to
  static uint8_t RegisterSelect=0;


/****************************************************************************
 Function
   LCD_HWInit
 Parameters
   None
 Returns
   Nothing
 Description
   Initializes the port hardware necessary to write to the LCD
 Notes
   This implementation uses the lower level shift register library so
   it simply calls the init for that library
 Author
   J. Edward Carryer, 10/12/15, 15:22
****************************************************************************/
void LCD_HWInit(void){
  // init the shift Register module
	//directly call the function written in part 2
	SR_Init();

}

/****************************************************************************
 Function
   LCD_TakeInitStep
 Parameters
   None
 Returns
   uint16_t the number of uS to delay before the next step
 Description
   steps through the initialization value array with each call and send the
   initialization value to the LCD and returns the time to delay until the 
   nxt step
 Notes
   
 Author
   J. Edward Carryer, 10/12/15, 15:22
****************************************************************************/
uint16_t LCD_TakeInitStep(void){
  uint16_t CurrentInitValue;
  uint16_t Delay;
  // for keeping track of the initialization step
  // 0 is a special case of powerup, then it takes on 1 to NUM_INIT_STEPS       
  static uint8_t CurrentStep = 0; //because this is static, it only initializes once

  // if we have completed the initialization steps, 
	if (CurrentStep>NUM_INIT_STEPS){
    //set Delay value to zero
		Delay=0;
	}
  // else, we are still intializing
	else{
    // if we are just getting started, this is a special case and we inc
    // the CurrrentStep and set the Delay to the first entry in the 
    //InitDelays table
		if (CurrentStep==0){
			CurrentStep=CurrentStep+1;
			Delay=InitDelays[0];//I assume 0-th element in the array is the first entry
		}
    //else
		else{
        // normal step, so grab the correct init value into CurrentInitValue
			CurrentInitValue=InitValues[CurrentStep-1]; //arrays are zero-indexed, so this takes the first entry of InitValues if CurrentStep==1
        // check to see if this is a 4-bit or 8-bit write and do the right kind
			if ((CurrentInitValue & USE_4_BIT_WRITE)>0){
			//first four entries are 16 bits, and the 8 bits ones & with 16 bits ones will give us 0
				LCD_WriteCommand4( (uint8_t) (CurrentInitValue & (~USE_4_BIT_WRITE))); //basically zero out the extra 1 at bit 15, then cast it to the correct type
			}
			else{
				LCD_WriteCommand8((uint8_t) CurrentInitValue);
			}
        // grab the correct delay for this step
			Delay=InitDelays[CurrentStep];
    // set up CurrentStep for next call
			CurrentStep=CurrentStep+1;
   } //end the else for normal step
 }//end the else for indicating still initializing
  return Delay;
}


/****************************************************************************
 Function
   LCD_WriteCommand4
 Parameters
   uint8_t NewData; the 4 LSBs are written
 Returns
   Nothing
 Description
   clears the register select bit to select the command register then
   writes the 4 bits of data, then pulses (raises, then lowers) the enable 
   line on the LCD
 Notes
   This implementation uses the lower level shift register library so
   it calls that library to change the value of the pins connected
   to the LCD
 Author
   J. Edward Carryer, 10/12/15, 16:18
****************************************************************************/
void LCD_WriteCommand4(uint8_t NewData){
  // clear the register select bit
		LCD_RegisterSelect(0);//put 0 here to indicate it's data, not instruction
	//whole reason we have separate WriteCommand4 function is because we didn't set RegisterSelect in Write4

	LCD_Write4(NewData);
  
}

/****************************************************************************
 Function
   LCD_WriteCommand8
 Parameters
   uint8_t NewData; 
 Returns
   Nothing
 Description
   clears the register select bit to select the command register then
   writes all 8 bits of data, using 2 4-bit writes
 Notes
   This implementation uses the lower level shift register library so
   it calls that library to change the value of the pins connected
   to the LCD
 Author
   J. Edward Carryer, 10/12/15, 16:26
****************************************************************************/
void LCD_WriteCommand8(uint8_t NewData){
  // set the register select bit
	LCD_RegisterSelect(0);//put 0 here to indicate it's data, not instruction
	//whole reason we have separate WriteCommand8 function is because we didn't set RegisterSelect in Write8
	
  // write all 8 bits to the shift register in 2 4-bit writes
	LCD_Write8(NewData);
}

/****************************************************************************
 Function
   LCD_WriteData8
 Parameters
   uint8_t NewData; 
 Returns
   Nothing
 Description
   sets the register select bit to select the data register then
   writes all 8 bits of data, using 2 4-bit writes
 Notes
   This implementation uses the lower level shift register library so
   it calls that library to change the value of the pins connected
   to the LCD
 Author
   J. Edward Carryer, 10/12/15, 16:28
****************************************************************************/
void LCD_WriteData8(uint8_t NewData){
  // set the register select bit
	LCD_RegisterSelect(1);//put 1 here to indicate it's data, not instruction
	//whole reason we have separate WriteData8 function is because we didn't set RegisterSelect in Write8
	
  // write all 8 bits to the shift register in 2 4-bit writes
	LCD_Write8(NewData);
}

//spin off to clean display
void LCD_CleanDisplay(void){
  // set the register select bit
	LCD_RegisterSelect(0);//clean display is an instruction
	//whole reason we have separate WriteData8 function is because we didn't set RegisterSelect in Write8

  // write all 8 bits to the shift register in 2 4-bit writes
	LCD_Write8(0x01); //only D0 is high, everything else low
}


//********************************
// thses functions are private to the module (these?)
//********************************
/****************************************************************************
 Function
   LCD_Write4
 Parameters
   uint8_t NewData; the 4 LSBs are written
 Returns
   Nothing
 Description
   writes the 4 bits of data, then pulses (raises, then lowers) the enable 
   line on the LCD
 Notes
   This implementation uses the lower level shift register library so
   it calls that library to change the value of the pins connected
   to the LCD
 Author
   J. Edward Carryer, 10/12/15, 15:58
****************************************************************************/
static void LCD_Write4(uint8_t NewData){
  // put the 4 bits of data onto the LCD data lines
	//I assume we are interested in the LSB
	LCD_SetData4(NewData);
  // pulse the enable line to complete the write
	LCD_PulseEnable();
}

/****************************************************************************
 Function
   LCD_Write8
 Parameters
   uint8_t NewData; all 8 bits are written
 Returns
   Nothing
 Description
   writes the 8 bits of data, pulssing the Enable line between the 4 bit writes
 Notes
   This implementation uses the lower level shift register library so
   it calls that library to change the value of the pins connected
   to the LCD
 Author
   J. Edward Carryer, 10/12/15, 15:58
****************************************************************************/
static void LCD_Write8(uint8_t NewData){
	uint8_t NewDataCopy;
	NewDataCopy=NewData;
	uint8_t MSBHalf;
	uint8_t LSBHalf;
	MSBHalf = NewDataCopy & (0xF0); //11110000
	LSBHalf = NewDataCopy & (0x0F); //00001111
  // put the 4 MSBs of data onto the LCD data lines
	LCD_SetData4(MSBHalf>>4);//because only the 4 LSBS are used
  // pulse the enable line to complete the write
	LCD_PulseEnable();
  // put the 4 LSBs of data onto the LCD data lines
	LCD_SetData4(LSBHalf);
  // pulse the enable line to complete the write
	LCD_PulseEnable();
}

/****************************************************************************
 Function
   LCD_RegisterSelect
 Parameters
   uint8_t Which; Should be either LCD_COMMAND or LCD_DATA
 Returns
   Nothing
 Description
   sets the port bit that controls the register select to the requested value
 Notes
   This implementation uses the lower level shift register library so
   it calls that library to change the value of the register select port bit
 Author
   J. Edward Carryer, 10/12/15, 15:28
****************************************************************************/
static void LCD_RegisterSelect(uint8_t WhichReg){
//  uint8_t CurrentValue;
  RegisterSelect = WhichReg;//0 means instruction, 1 means data
	uint8_t CurrentRegisterValue=SR_GetCurrentRegister();
	if(RegisterSelect == 1){
	  SR_Write(CurrentRegisterValue|=BIT1HI); //at the end, we will write this register value to the LCD, so we know if it's data or instruction
	}
	else{
		SR_Write(CurrentRegisterValue&=BIT1LO);
	}
}

/****************************************************************************
 Function
   LCD_SetData4
 Parameters
   uint8_t NewData; only the 4 LSBs are used
 Returns
   Nothing
 Description
   sets the 4 data bits to the LCD to the requested value and places the
   register select value in the correct Bit position (bit 1)
 Notes
   This implementation uses the lower level shift register library so
   it calls that library to change the value of the data pins
 Author
   J. Edward Carryer, 10/12/15, 15:42
****************************************************************************/
static void LCD_SetData4(uint8_t NewData){
  uint8_t CurrentRegisterValue;
	uint8_t NewDataCopy;
	NewDataCopy=NewData;
  // get the current value of the port so that we can preserve the other
  // bit states 
	CurrentRegisterValue=SR_GetCurrentRegister(); //because we don't want to mess up the instruction bit and R/W bit
  // insert the current state of RegisterSelect into bit 1
	
	/*if(RegisterSelect == 1){
	  CurrentRegisterValue|=BIT1HI; //at the end, we will write this register value to the LCD, so we know if it's data or instruction
	}
	else{
		CurrentRegisterValue&=BIT1LO;
	}
	//moved to LCD_RegisterSelect();
	*/
  // put the 4 LSBs into the 4 MSB positions to apply the data to them
  // correct LCD inputs while preserving the states of the other bits
  // now write the new value to the shift register
	
	//need to clear out top four bits
	//only want to modify the most significant 4 bits, Q4 to Q7
	SR_Write( ((CurrentRegisterValue)& (LSB_MASK)) | (NewDataCopy<<4)); //shift the newdata 4 bits over, and or with the current values, then write it back
	                                                      //with register select modified of course
	                                                      //that 0xF0 is meant to be 00001111, want to clear the data bits first before writing the data
}

/****************************************************************************
 Function
   LCD_PulseEnable
 Parameters
   Nothing
 Returns
   Nothing
 Description
   pulses (raises, then lowers) the enable line on the LCD
 Notes
   This implementation uses the lower level shift register library so
   it calls that library to change the value of the data pin connected
   to the Enable pin on the LCD (bit 0 on the shift register)
 Author
   J. Edward Carryer, 10/12/15, 15:42
****************************************************************************/
static void LCD_PulseEnable(void){
  uint8_t CurrentRegisterValue;
  //want bit 0 to be HIGH and then LOW
  // get the current value of the port so that we can preserve the other
  // bit states
	CurrentRegisterValue=SR_GetCurrentRegister();
  // set the LSB of the byte to be written to the shift register
  // now write the new value to the shift register
	SR_Write(CurrentRegisterValue|BIT0HI);
  // clear the LSB of the byte to be written to the shift register
  // now write the new value to the shift register
	//SR_Write(CurrentRegisterValue^BIT0HI); //yes, exclusive or
	SR_Write( (CurrentRegisterValue & (BIT0LO))); //&11111110
}



//---------------TEST HARRNESS-----------------
#ifdef TEST
/* test Harness for testing this module */

int main(void)
{
	#include "termio.h"
  TERMIO_Init();
  puts("\r\n In test harness for LCD_Write\r\n");
	puts("Doing LCD_HWInit Next");
	getchar();
	LCD_HWInit();
	puts("Doing LCD_Register Next");
	getchar();
	uint8_t RSValue=1;
	LCD_RegisterSelect(RSValue); 
	puts("Doing LCD_SetData4 Next");
	getchar();
	LCD_SetData4(10); //expect to see 00001010
	puts("Doing LCD_PulseEnable Next");
	getchar();
	LCD_PulseEnable(); //now I should see 10100001 coming from shifting 10 four bits over and having the register select value
	puts("Doing LCD_WriteData8 Next");
	getchar();
	
	//init
	puts("start intialization");
	/*
	LCD_WriteCommand4(0x03);
	getchar();
	LCD_WriteCommand4(0x03);
	getchar();
	LCD_WriteCommand4(0x03);
	getchar();
	LCD_WriteCommand4(0x02);
	getchar();	
	LCD_WriteCommand8(0x20);
	getchar();
	LCD_WriteCommand8(0x08);
	getchar();
	LCD_WriteCommand8(0x01);
	getchar();
	LCD_WriteCommand8(0x07);
	getchar();
	LCD_WriteCommand8(0x0f);
	getchar();
	LCD_WriteCommand8(0x97);
	getchar();
	*/
	uint16_t tempDelay;
	uint8_t i=0;
	for (i=0;i<11;i++){
		tempDelay=LCD_TakeInitStep();
		getchar();
	}
	puts("finish intialization");
	
	char charToPrint;
	while(1){//infinite loop so it keeps printing
		charToPrint=getchar();
		LCD_WriteData8((uint8_t) charToPrint);
		//LCD_WriteData8('a');
	}
return 0;
}
#endif
