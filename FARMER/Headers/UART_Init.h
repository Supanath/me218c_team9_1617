/****************************************************************************
 
  Header file for UART Initialization

 ****************************************************************************/

#ifndef UART_INIT_H
#define UART_INIT_H

#include "ES_Configure.h"
#include "ES_Types.h"

// Public Function Prototypes

void Init_UART(void);
void Init_UART_ModuleFive();
#endif /* ServTemplate_H */

