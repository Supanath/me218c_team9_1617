/****************************************************************************
 
  Header file for FARMER RX Service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef FARMER_RX_H
#define FARMER_RX_H

#include "ES_Configure.h"
#include "ES_Types.h"

// Public Function Prototypes

bool InitFarmerRX ( uint8_t Priority );
bool PostFarmerRX( ES_Event ThisEvent );
ES_Event RunFarmerRX( ES_Event ThisEvent );
uint8_t* GetIMU_Status();

#endif /* ServTemplate_H */

