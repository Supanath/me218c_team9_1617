/****************************************************************************
 
  Header file for FARMER TOP STATE MACHINE 
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef FarmerTopSM_H
#define FarmerTopSM_H

#include "ES_Configure.h"
#include "ES_Types.h"

typedef enum {WaitToPair, WaitForPairResponse, Paired} FarmerTopState_t;

typedef enum {WaitFor7E, WaitForMSBLen, WaitForLSBLen, GetDataPacket, GetCheckSum} FarmerRXState_t;

// Public Function Prototypes

bool InitFarmerTopSM ( uint8_t Priority );
bool PostFarmerTopSM( ES_Event ThisEvent );
ES_Event RunFarmerTopSM( ES_Event ThisEvent );
void FARMER_UART1__ISR(void);
uint32_t get_encoder1_count(void);
double get_RPM_Encoder1(void);
void StartOneShot_Encoder1(void);
void InitInputCapture_Encoder1( void );
void InitOneShotInt_Encoder1(void);
void InputCaptureResponse_Encoder1(void);
void OneShotIntResponse_Encoder1(void);
uint8_t GetRXBuffer(int index);
#endif /* FarmerTopSM_H */

