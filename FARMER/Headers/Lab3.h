/****************************************************************************
 
  Header file for template service 
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef LCD_Control_H
#define LCD_Control_H

#include "ES_Types.h"

// Public Function Prototypes

bool LCD_ControlInit ( uint8_t Priority );
bool PostLCD_Control( ES_Event ThisEvent );
ES_Event LCD_ControlRun( ES_Event ThisEvent );


#endif /* LCD_Control_H */

