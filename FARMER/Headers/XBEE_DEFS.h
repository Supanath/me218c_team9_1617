/*
   standard bit defintions to make things more readable for Xbee
*/

/* the conditional keeps this from being included more than once */
#ifndef XBEE_DEFS_H
#define XBEE_DEFS_H

#define REG_2_PAIR	    0x01
#define	PAIR_ACK				0x02
#define ENCR_KEY				0x03
#define	CTRL						0x04
#define	ENCR_RESET	    0x05
#define STATUS					0x00

#endif  /* XBEE_DEFS_H */

