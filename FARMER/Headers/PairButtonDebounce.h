/****************************************************************************
 
  Header file for Test Harness Service0 
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef PairButtonDebounce_H
#define PairButtonDebounce_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum { Debouncing, Ready2Sample} DebounceButtonState_t ;

// Public Function Prototypes

bool InitializeButtonDebounce(uint8_t Priority );
bool PostButtonDebounceService(ES_Event ThisEvent );
ES_Event RunButtonDebounceSM(ES_Event ThisEvent );
bool CheckButtonEvents(void);
#endif /* ButtonDebounce_Service_H */

